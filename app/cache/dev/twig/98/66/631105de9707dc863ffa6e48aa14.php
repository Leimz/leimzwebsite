<?php

/* LeimzNewsBundle:News:consulter.html.twig */
class __TwigTemplate_9866631105de9707dc863ffa6e48aa14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "new"));
        foreach ($context['_seq'] as $context["_key"] => $context["array"]) {
            // line 4
            $context["new"] = $this->getContext($context, "array");
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['array'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo " ";
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getContext($context, "new"), "titre")), "html", null, true);
        echo " ";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "\t<h1 class=\"titre\">";
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getContext($context, "new"), "titre")), "html", null, true);
        echo "</h1>
\t
\t";
        // line 12
        if (($this->getAttribute($this->getContext($context, "new"), "brouillon") == 0)) {
            // line 13
            echo "\t<div class=\"news_boutons\">
\t";
            // line 14
            if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
                // line 15
                echo "\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_commenter", array("news" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
                echo "\" >Commenter cette news.</a>
\t";
            } else {
                // line 17
                echo "\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_login"), "html", null, true);
                echo "\">Connectez vous pour commenter cette news !</a>
\t";
            }
            // line 19
            echo "\t";
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 20
                echo "\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_supprimer", array("id" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
                echo "\" >Supprimer la news.</a>
\t\t<a href=\"";
                // line 21
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_verrouiller", array("id" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
                echo "\" >Verrouiller les commentaires.</a>
\t\t<a href=\"";
                // line 22
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_editer", array("id" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
                echo "\" >Éditer cette news.</a>
\t\t
\t";
            }
            // line 25
            echo "\t</div>
\t";
        } else {
            // line 27
            echo "\t\t<em>Attention, cette news n'a pas encore été publiée, ceci est seulement une prévisualisation.</em>
\t";
        }
        // line 29
        echo "\t
\t<div class=\"news_image\">
\t\t<img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/leimznews.jpg"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "new"), "titre"), "html", null, true);
        echo "\" />
\t</div>
\t
\t<div class=\"news_corps\">
\t\t<p class=\"date\">Publiée le ";
        // line 35
        echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getContext($context, "new"), "date"), "d/m/Y"), "html", null, true);
        echo " par ";
        $context["i"] = 0;
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "new"), "auteur"));
        foreach ($context['_seq'] as $context["_key"] => $context["auteur"]) {
            $context["i"] = ($this->getContext($context, "i") + 1);
            if (($this->getContext($context, "i") > 1)) {
                echo ", ";
            }
            echo "<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_profile_show"), "html", null, true);
            echo "?pseudo=";
            echo twig_escape_filter($this->env, $this->getContext($context, "auteur"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getContext($context, "auteur"), "html", null, true);
            echo "</a>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['auteur'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo "</p>

\t\t<p>
\t\t\t<p class=\"news_contenu\">";
        // line 38
        echo $this->env->getExtension('fm_bbcode')->filter($this->getAttribute($this->getContext($context, "new"), "contenu"), "default_filter");
        echo "</p>
\t\t
\t\t\t<p class=\"date edit\">Dernière mise à jour le ";
        // line 40
        echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getContext($context, "new"), "dateEdit"), "D m Y"), "html", null, true);
        echo "</p>
\t\t</p>
\t</div>
\t
\t";
        // line 44
        if (($this->getAttribute($this->getContext($context, "new"), "brouillon") == 0)) {
            // line 45
            echo "\t\t<div class=\"news_boutons\">
\t\t";
            // line 46
            if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
                // line 47
                echo "\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_commenter", array("news" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
                echo "\" >Commenter cette news.</a>
\t\t";
            } else {
                // line 49
                echo "\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_login"), "html", null, true);
                echo "\">Connectez vous pour commenter cette news !</a>
\t\t";
            }
            // line 51
            echo "\t\t";
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 52
                echo "\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_supprimer", array("id" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
                echo "\" >Supprimer la news.</a>
\t\t\t<a href=\"";
                // line 53
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_verrouiller", array("id" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
                echo "\" >Verrouiller les commentaires.</a>
\t\t\t<a href=\"";
                // line 54
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_editer", array("id" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
                echo "\" >Éditer cette news.</a>
\t\t";
            }
            // line 56
            echo "\t\t</div>
\t";
        } else {
            // line 58
            echo "\t\t<em>Attention, cette news n'a pas encore été publiée, ceci est seulement une prévisualisation.</em>
\t";
        }
        // line 60
        echo "\t
\t";
        // line 61
        if (($this->getAttribute($this->getContext($context, "new"), "brouillon") == 0)) {
            // line 62
            echo "\t<table class=\"comments_table\">
\t\t\t";
            // line 63
            if ((twig_length_filter($this->env, $this->getContext($context, "pagination")) > 10)) {
                // line 64
                echo "\t\t\t\t<tr class=\"pagerfanta pagination_comment\">
\t\t\t\t<td colspan=\"2\">
\t\t\t\t\t";
                // line 66
                echo $this->env->getExtension('pagerfanta')->renderPagerfanta($this->getContext($context, "pagination"), "default_translated");
                echo "
\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t";
            }
            // line 70
            echo "\t\t\t
\t\t\t<tr class=\"comment_titres\">
\t\t\t\t<td class=\"comment_titre comment_auteur\">Auteur</td>
\t\t\t\t<td class=\"comment_titre\">Commentaire</td>
\t\t\t</tr>
\t\t
\t\t\t";
            // line 76
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "pagination"));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["commentaire"]) {
                // line 77
                echo "\t\t\t\t";
                if ((($this->getAttribute($this->getContext($context, "commentaire"), "supprimer") == 1) && (!$this->env->getExtension('security')->isGranted("ROLE_ADMIN")))) {
                    // line 78
                    echo "\t\t\t\t<tr class=\"comment_deleted_line\">
\t\t\t\t\t<td colspan=\"2\">
\t\t\t\t\t\t<a class=\"link_auteur\" href=\"";
                    // line 80
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_profile_show"), "html", null, true);
                    echo "?pseudo=";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "html", null, true);
                    echo "</a> | Commentaire supprimé.
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t";
                } else {
                    // line 84
                    echo "\t\t\t\t<tr class=\"comment_line\">
\t\t\t\t\t<td class=\"col_auteur\"><a class=\"link_auteur\" href=\"";
                    // line 85
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_profile_show"), "html", null, true);
                    echo "?pseudo=";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "html", null, true);
                    echo "<br/>
                                                                    ";
                    // line 86
                    if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "complement"), "avatar") != "")) {
                        // line 87
                        echo "                                                                            ";
                        if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "complement"), "typeAvatar") == 0)) {
                            // line 88
                            echo "                                                                                <img src=\"";
                            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "complement"), "avatar"), "html", null, true);
                            echo "\" alt=\"avatar\" title=\"avatar\" width=\"120px\" height= \"120px\"/>
                                                                            ";
                        } elseif (($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "complement"), "typeAvatar") == 1)) {
                            // line 90
                            echo "                                                                                <img src=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("bundles/leimzutilisateur/images/avatars/" . $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "complement"), "avatar")) . ".jpg")), "html", null, true);
                            echo "\" alt=\"avatar\" title=\"avatar\" width=\"120px\" height= \"120px\" />
                                                                            ";
                        }
                        // line 92
                        echo "                                                                     ";
                    }
                    echo "</a>
\t\t\t\t\t\t";
                    // line 93
                    if ((twig_date_format_filter($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "complement"), "birthdate"), "d/m/Y") != "30/11/-0001")) {
                        echo "<div class=\"auteur_date\" >Né(e) le ";
                        echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "complement"), "birthdate"), "d/m/Y"), "html", null, true);
                        echo ".</div>";
                    }
                    // line 94
                    echo "\t\t\t\t\t\t";
                    if (($this->env->getExtension('security')->isGranted("ROLE_ADMIN") && ($this->getAttribute($this->getContext($context, "commentaire"), "supprimer") != 1))) {
                        // line 95
                        echo "\t\t\t\t\t\t\t<a class=\"comment_suppr\" href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_editer_commentaire", array("id" => $this->getAttribute($this->getContext($context, "commentaire"), "id"))), "html", null, true);
                        echo "\">Éditer</a>
\t\t\t\t\t\t\t<a class=\"comment_suppr\" href=\"";
                        // line 96
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_supprimer_commentaire", array("id" => $this->getAttribute($this->getContext($context, "commentaire"), "id"))), "html", null, true);
                        echo "\">Supprimer</a>
                                                ";
                    } else {
                        // line 98
                        echo "                                                        <span class=\"erreur\">Commentaire supprimé.</span>
\t\t\t\t\t\t";
                    }
                    // line 100
                    echo "\t\t\t\t\t\t";
                    if (((($this->getAttribute($this->getContext($context, "commentaire"), "auteur") == $this->getAttribute($this->getContext($context, "app"), "user")) && (!$this->env->getExtension('security')->isGranted("ROLE_ADMIN"))) && ($this->getAttribute($this->getContext($context, "commentaire"), "supprimer") != 1))) {
                        // line 101
                        echo "\t\t\t\t\t\t\t<a class=\"comment_suppr\" href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_supprimer_commentaire", array("id" => $this->getAttribute($this->getContext($context, "commentaire"), "id"))), "html", null, true);
                        echo "\">Supprimer</a>
\t\t\t\t\t\t";
                    }
                    // line 102
                    echo "\t
\t\t\t\t\t</td>
\t\t\t\t\t<td class=\"col_content\">
                                            <div class=\"comment_date\" >Le ";
                    // line 105
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getContext($context, "commentaire"), "date"), "d/m/Y"), "html", null, true);
                    echo ".</div>
                                            <div class=\"comment_content\">";
                    // line 106
                    echo $this->env->getExtension('fm_bbcode')->filter($this->getAttribute($this->getContext($context, "commentaire"), "contenu"), "default_filter");
                    echo "</div>
\t\t\t\t\t\t";
                    // line 107
                    if (($this->getAttribute($this->getContext($context, "commentaire"), "date") != $this->getAttribute($this->getContext($context, "commentaire"), "dateEdit"))) {
                        // line 108
                        echo "\t\t\t\t\t\t\t<div class=\"comment_dateEdit\"><em>Dernière édition le ";
                        echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getContext($context, "commentaire"), "dateEdit"), "d/m/Y"), "html", null, true);
                        echo ".</em></div>
\t\t\t\t\t\t";
                    }
                    // line 110
                    echo "                                                ";
                    if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "complement"), "signature") != "")) {
                        // line 111
                        echo "\t\t\t\t\t\t\t<div class=\"comment_signature\">";
                        echo $this->env->getExtension('fm_bbcode')->filter($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "commentaire"), "auteur"), "complement"), "signature"), "default_filter");
                        echo "</div>
\t\t\t\t\t\t";
                    }
                    // line 113
                    echo "\t\t\t\t\t</td>   
\t\t\t\t</tr>
\t\t\t\t";
                }
                // line 116
                echo "\t\t\t";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 117
                echo "\t\t\t\t<tr><td><center>Aucun commentaire pour le moment.</center></td></tr>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['commentaire'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 119
            echo "\t\t\t";
            if ((twig_length_filter($this->env, $this->getContext($context, "pagination")) > 10)) {
                // line 120
                echo "\t\t\t\t<tr class=\"pagerfanta pagination_comment\">
\t\t\t\t<td colspan=\"2\">
\t\t\t\t\t";
                // line 122
                echo $this->env->getExtension('pagerfanta')->renderPagerfanta($this->getContext($context, "pagination"), "default_translated");
                echo "
\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t";
            }
            // line 126
            echo "\t\t\t</table>
\t";
        }
        // line 128
        echo "


";
    }

    public function getTemplateName()
    {
        return "LeimzNewsBundle:News:consulter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
