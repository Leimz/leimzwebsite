<?php

/* CoreSphereConsoleBundle:Console:command_list.json.twig */
class __TwigTemplate_984c452095bf4f075955cbb97bac3180 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_jsonencode_filter(twig_get_array_keys_filter($this->getContext($context, "commands")));
        echo "
";
    }

    public function getTemplateName()
    {
        return "CoreSphereConsoleBundle:Console:command_list.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
