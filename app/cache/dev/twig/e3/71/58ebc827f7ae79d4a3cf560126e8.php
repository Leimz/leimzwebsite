<?php

/* LeimzPagesBundle:Whatsthat:ajouter.html.twig */
class __TwigTemplate_e37158ebc827f7ae79d4a3cf560126e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Ajout/Edition d'une partie";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    <form method=\"POST\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo " action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("whatsthat_ajouter"), "html", null, true);
        echo "\" >
        ";
        // line 7
        $this->env->loadTemplate("::bbcode.html.twig")->display($context);
        // line 8
        echo "
        ";
        // line 9
        echo $this->env->getExtension('form')->renderWidget($this->getContext($context, "form"));
        echo "

        <input type=\"submit\" value=\"Publier\" />
    
    </form>

";
    }

    public function getTemplateName()
    {
        return "LeimzPagesBundle:Whatsthat:ajouter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
