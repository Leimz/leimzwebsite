<?php

/* CoreSphereConsoleBundle:Console:result.json.twig */
class __TwigTemplate_729e54d0de34eb9b4f0dbd2df06d89cf extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "{
    \"command\" :    ";
        // line 3
        echo twig_jsonencode_filter(twig_escape_filter($this->env, $this->getContext($context, "input")));
        echo ",
    \"output\" :     ";
        // line 4
        echo twig_jsonencode_filter($this->getContext($context, "output"));
        echo ",
    \"environment\": ";
        // line 5
        echo twig_jsonencode_filter(twig_escape_filter($this->env, $this->getContext($context, "environment")));
        echo "
}
";
    }

    public function getTemplateName()
    {
        return "CoreSphereConsoleBundle:Console:result.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
