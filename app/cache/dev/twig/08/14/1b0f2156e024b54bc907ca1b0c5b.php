<?php

/* LeimzUtilisateurBundle:Profile:showMember.html.twig */
class __TwigTemplate_08141b0f2156e024b54bc907ca1b0c5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if ($this->getContext($context, "user")) {
            // line 5
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.showMember.title", array("%username%" => $this->getAttribute($this->getContext($context, "user"), "username")), "LeimzUtilisateurBundle"), "html", null, true);
            echo "
    ";
        } else {
            // line 7
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.showMember.undefined.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "
    ";
        }
    }

    // line 12
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 13
        echo "
    ";
        // line 14
        if ($this->getContext($context, "user")) {
            // line 15
            echo "        <h1 class=\"title show_profile_title\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.showMember.title", array("%username%" => $this->getAttribute($this->getContext($context, "user"), "username")), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</h1>
\t\t
\t\t ";
            // line 17
            if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
                // line 18
                echo "        \t\t<p>
        \t\t\t<a href=\"";
                // line 19
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_lock_membre", array("membre" => $this->getAttribute($this->getContext($context, "user"), "username"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.info.lock_membre", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "</a><br/>
        \t\t\t<a href=\"";
                // line 20
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_modifierMembre", array("membre" => $this->getAttribute($this->getContext($context, "user"), "username"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.info.edit_profile_member", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "</a>
        \t\t</p>
        \t";
            }
            // line 23
            echo "        \t
        <div class=\"profile_avatar\">
    \t";
            // line 25
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "avatar", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "avatar") != ""))) {
                // line 26
                echo "    \t\t";
                if (($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "typeAvatar") == 0)) {
                    // line 27
                    echo "    \t\t\t<img src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "avatar"), "html", null, true);
                    echo "\" alt=\"avatar\" title=\"avatar\" width=\"120px\" height= \"120px\"/>
    \t\t";
                } elseif (($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "typeAvatar") == 1)) {
                    // line 29
                    echo "    \t\t\t<img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("bundles/leimzutilisateur/images/avatars/" . $this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "avatar")) . ".jpg")), "html", null, true);
                    echo "\" alt=\"avatar\" title=\"avatar\" width=\"120px\" height= \"120px\" />
    \t\t";
                }
                // line 31
                echo "    \t";
            }
            // line 32
            echo "        </div>              
        <div class=\"fos_user_user_show\">
            <p>";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.showMember.username", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "username"), "html", null, true);
            echo "</p>
            ";
            // line 35
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "pubEmail", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "pubEmail") == 1))) {
                // line 36
                echo "                <p>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.showMember.email", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo " : ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "email"), "html", null, true);
                echo "</p>
            ";
            }
            // line 38
            echo "            <p>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.showMember.last_login", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getContext($context, "user"), "lastLogin"), "d/m/Y à H:i"), "html", null, true);
            echo "</p>
            
            <div>
    \t\t";
            // line 41
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "birthdate", array(), "any", true, true) && (twig_date_format_filter($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "birthdate"), "d/m/Y") != "30/11/-0001"))) {
                // line 42
                echo "    \t\t\t<p>Date de naissance : ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "birthdate"), "d/m/Y"), "html", null, true);
                echo "</p>
    \t\t";
            }
            // line 44
            echo "    \t\t";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "sexe", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "sexe") != "null"))) {
                // line 45
                echo "    \t\t\t<p>Sexe : ";
                if (($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "sexe") == 0)) {
                    echo "Homme";
                } else {
                    echo "Femme";
                }
                echo "</p>
    \t\t";
            }
            // line 47
            echo "    \t\t";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "signature", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "signature") != ""))) {
                // line 48
                echo "    \t\t\t<p>Signature : <br/>";
                echo $this->env->getExtension('fm_bbcode')->filter($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "signature"), "default_filter");
                echo "</p>
    \t\t";
            }
            // line 50
            echo "    \t\t";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "description", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "description") != ""))) {
                // line 51
                echo "    \t\t\t<p>Description : ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "description"), "html", null, true);
                echo "</p>
    \t\t";
            }
            // line 53
            echo "            </div>
        </div>
        
    ";
        } else {
            // line 57
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.showMember.undefined.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "
    ";
        }
        // line 59
        echo "
";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Profile:showMember.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
