<?php

/* ::bbcode.html.twig */
class __TwigTemplate_5337838e526fad3130939adf5ba86095 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'bbcode' => array($this, 'block_bbcode'),
        );
    }

    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('bbcode', $context, $blocks);
        // line 42
        echo "

<!-- <li><img src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("leimznews/images/bbcode/gras.gif"), "html", null, true);
        echo "\" title=\"sourire\" alt=\"sourire\" onclick=\"javascript:smilies(' :) ');return(false)\" /></li> -->";
    }

    // line 1
    public function block_bbcode($context, array $blocks = array())
    {
        // line 2
        echo "<div class=\"bbcode\">
    <ul class=\"bbcodeListe\" >
        <li class=\"bbcodeElementMEP\"><img src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/bbcode/gras.gif"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.gras", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.gras", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" onclick=\"javascript:bbcode('[b]', '[/b]');return(false)\" /></li>
        <li class=\"bbcodeElementMEP\"><img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/bbcode/italique.gif"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.italique", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.italique", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" onclick=\"javascript:bbcode('[i]', '[/i]');return(false)\" /></li>
        <li class=\"bbcodeElementMEP\"><img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/bbcode/souligne.gif"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.souligne", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.souligne", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" onclick=\"javascript:bbcode('[u]', '[/u]');return(false)\" /></li>
        <li class=\"bbcodeElementMEP\"><img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/bbcode/barre.gif"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.barre", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.barre", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" onclick=\"javascript:bbcode('[s]', '[/s]');return(false)\" /></li>
        <li class=\"bbcodeElementMEP\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/bbcode/url.gif"), "html", null, true);
        echo "\" alt=\"lien\" title=\"lien\" onclick=\"javascript:bbcode('[url]', '[/url]');return(false)\" /></li>
        <li class=\"bbcodeElementMEP\"><img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/bbcode/image.gif"), "html", null, true);
        echo "\" alt=\"image\" title=\"image\" onclick=\"javascript:bbcode('[img]', '[/img]');return(false)\" /></li>
        <li class=\"bbcodeElementMEP bbcodeListe bbcodeSize\"><img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/bbcode/size.gif"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.size", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.size", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" />
            <ul class=\"bbcodeListeSize\">
                <li class=\"bbcodeListeElement bbcodeSizeTresPetit\" onclick=\"javascript:bbcode('[size=&quot;9&quot;]', '[/size]');return(false)\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.sizes.tres_petit", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeSizePetit\" onclick=\"javascript:bbcode('[size=&quot;11&quot;]', '[/size]');return(false)\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.sizes.petit", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeSizeNormal\" onclick=\"javascript:bbcode('[size=&quot;18&quot;]', '[/size]');return(false)\">";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.sizes.grand", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeSizeGrand\" onclick=\"javascript:bbcode('[size=&quot;24&quot;]', '[/size]');return(false)\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.sizes.tres_grand", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
            </ul>
        </li>
        <li class=\"bbcodeElementMEP bbcodeListe bbcodeFont\"><img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/bbcode/font.gif"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.font", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.font", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" />
            <ul class=\"bbcodeListeFont\">
                <li class=\"bbcodeListeElement bbcodeFontArial\" onclick=\"javascript:bbcode('[font=&quot;arial&quot;]', '[/font]');return(false)\">arial</li>
                <li class=\"bbcodeListeElement bbcodeFontTimes\" onclick=\"javascript:bbcode('[font=&quot;times new roman&quot;]', '[/font]');return(false)\">times new roman</li>
                <li class=\"bbcodeListeElement bbcodeFontCourier\" onclick=\"javascript:bbcode('[font=&quot;courier&quot;]', '[/font]');return(false)\">courier</li>
                <li class=\"bbcodeListeElement bbcodeFontComics\" onclick=\"javascript:bbcode('[font=&quot;Comic sans ms&quot;]', '[/font]');return(false)\">comic sans ms</li>
            </ul>
        </li>
        <li class=\"bbcodeElementMEP bbcodeListe bbcodeColor\"><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/bbcode/color.gif"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.color", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.color", array(), "LeimzNewsBundle"), "html", null, true);
        echo "\" />
            <ul class=\"bbcodeListeColor\">
                <li class=\"bbcodeListeElement bbcodeColorBlack\" onclick=\"javascript:bbcode('[color=&quot;black&quot;]', '[/color]');return(false)\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.colors.noir", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeColorRed\" onclick=\"javascript:bbcode('[color=&quot;red&quot;]', '[/color]');return(false)\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.colors.rouge", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeColorPurple\" onclick=\"javascript:bbcode('[color=&quot;purple&quot;]', '[/color]');return(false)\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.colors.violet", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeColorBlue\" onclick=\"javascript:bbcode('[color=&quot;blue&quot;]', '[/color]');return(false)\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.colors.bleu", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeColorLightBlue\" onclick=\"javascript:bbcode('[color=&quot;lightblue&quot;]', '[/color]');return(false)\">";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.colors.bleu_clair", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeColorGreen\" onclick=\"javascript:bbcode('[color=&quot;green&quot;]', '[/color]');return(false)\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.colors.vert", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeColorDarkGreen\" onclick=\"javascript:bbcode('[color=&quot;darkgreen&quot;]', '[/color]');return(false)\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.colors.vert_fonce", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeColorOrange\" onclick=\"javascript:bbcode('[color=&quot;darkorange&quot;]', '[/color]');return(false)\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.colors.orange", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
                <li class=\"bbcodeListeElement bbcodeColorYellow\" onclick=\"javascript:bbcode('[color=&quot;yellow&quot;]', '[/color]');return(false)\">";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bbcode.colors.jaune", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</li>
            </ul>
        </li>
    </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "::bbcode.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
