<?php

/* LeimzUtilisateurBundle:Admin:listerMembres.html.twig */
class __TwigTemplate_4b0f56ea79e2d1078427cdd2e82d89d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'results' => array($this, 'block_results'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "\t\t";
        if (($this->getContext($context, "locked") == 0)) {
            // line 5
            echo "\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "
\t\t";
        } else {
            // line 7
            echo "\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.titleLocked", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "
\t\t";
        }
    }

    // line 33
    public function block_results($context, array $blocks = array())
    {
        // line 34
        echo "    \t";
        $this->env->loadTemplate("LeimzUtilisateurBundle:Admin:listeMembres.html.twig")->display(array_merge($context, array("membres" => $this->getContext($context, "membres"))));
        // line 35
        echo "    \t";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "


\t<h1 class=\"admin_title\">";
        // line 15
        if (($this->getContext($context, "locked") == 0)) {
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
        } else {
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.titleLocked", array(), "LeimzUtilisateurBundle"), "html", null, true);
        }
        echo "</h1>
\t
\t<form id=\"form_recherche\" class=\"rechercheMembre_form\" action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_utilisateur_rechercher"), "html", null, true);
        echo "\" method=\"post\">
\t\t
\t\t<label for=\"username\" class=\"rechercheMembre_label\" >";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.form.champ", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</label>
\t\t
\t\t<input type=\"submit\" class=\"rechercheMembre_submit\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.form.submit", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "\" />
\t\t
\t\t";
        // line 23
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "username"));
        echo "
\t\t
\t\t";
        // line 25
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
\t\t
\t</form>


\t<div class=\"loading\"></div>
\t
\t<div id=\"resultats_recherche\"> 
\t\t";
        // line 33
        $this->displayBlock('results', $context, $blocks);
        // line 36
        echo "\t</div>
<script>\t
\tvar oldmotcle = 'a';\t
\t
\tfunction vaChercher(){ 
    \$(\".loading\").show();
    var motcle = \$(\"#utilisateurajax_username\").val();
    var DATA = 'motcle=' + motcle;
    var DATA = DATA + '&locked=";
        // line 44
        echo twig_escape_filter($this->env, $this->getContext($context, "locked"), "html", null, true);
        echo "';
    
    if(oldmotcle != motcle)
    {
    \t\$.ajax({
        type: \"POST\",
        url: \"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_utilisateur_rechercher"), "html", null, true);
        echo "\",
        data: DATA,
        cache: false,
        success: function(data){
           \$('#resultats_recherche').html(data);
           \$(\".loading\").hide();
           var oldmotcle = motcle;
  \t\t\t}
  \t\t});
  \t}  
    setTimeout(\"vaChercher()\", 1000);
    return false;
}
vaChercher();
\$(\"#form_recherche\").submit(vaChercher()); 
</script>

";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Admin:listerMembres.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
