<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_c264618a29f7e9b826ab343c0fea250a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('subject', $context, $blocks);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
    }

    // line 1
    public function block_subject($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("confirm_email.subject", array(), "LeimzUtilisateurBundle"), "html", null, true);
    }

    // line 3
    public function block_body_text($context, array $blocks = array())
    {
        // line 5
        echo $this->env->getExtension('translator')->trans("confirm_email.hello", array(), "LeimzUtilisateurBundle");
        echo " ";
        echo $this->getAttribute($this->getContext($context, "user"), "username");
        echo " !

";
        // line 7
        echo $this->env->getExtension('translator')->trans("confirm_email.text", array(), "LeimzUtilisateurBundle");
        echo $this->getContext($context, "confirmationUrl");
        echo "

";
        // line 9
        echo $this->env->getExtension('translator')->trans("confirm_email.end", array(), "LeimzUtilisateurBundle");
        echo "
";
    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        // line 14
        echo "
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function isTraitable()
    {
        return true;
    }
}
