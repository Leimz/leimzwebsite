<?php

/* LeimzAdminBundle:Default:index.html.twig */
class __TwigTemplate_9b8b6cb8f2b720dfa6a83003bc121306 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzAdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("tableau.title", array(), "LeimzAdminBundle"), "html", null, true);
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "
        <h1 class=\"title admin_title\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("tableau.title", array(), "LeimzAdminBundle"), "html", null, true);
        echo "</h1>


<div class=\"admin_item\" id=\"admin_membre\">
\t<h3>Gestion des membres</h3>
\t<p><a href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_voir_membres"), "html", null, true);
        echo "\">Liste des membres actifs</a></p>
\t<p><a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_voir_membres_locked"), "html", null, true);
        echo "\">Liste des membres bloqués</a></p>
</div>

<div class=\"admin_item\" id=\"admin_news\">
\t<h3>Gestion des News</h3>
\t<p><a href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_ajouter"), "html", null, true);
        echo "\">Ecrire une nouvelle news</a></p>
\t<p><a href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_brouillons"), "html", null, true);
        echo "\" >Voir les brouillons de news (pour édition puis publication)</a></p>
</div>


";
    }

    public function getTemplateName()
    {
        return "LeimzAdminBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
