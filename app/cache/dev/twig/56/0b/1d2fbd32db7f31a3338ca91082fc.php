<?php

/* LeimzNewsBundle:Admin:brouillons.html.twig */
class __TwigTemplate_560b1d2fbd32db7f31a3338ca91082fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "News en brouillon";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "\t<h1>News en brouillon</h1>
\t
\t\t<p class=\"brouillons_explain\">Pour publier une news, décochez la case \"Mettre en brouillon\" lors de l'édition.</p>
\t\t<table class=\"brouillons_table\">
\t\t\t<tr class=\"brouillons_line_titre\">
\t\t\t\t<td class=\"brouillons_titre_col\">Titre</td>
\t\t\t\t<td class=\"brouillons_datetitre_col\">Date</td>
\t\t\t\t<td class=\"brouillons_auteurs_col\">Auteur(s)</td>
\t\t\t\t<td class=\"brouillons_actions_col\">Actions</td>
\t\t\t</tr>
\t
\t";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "news"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["new"]) {
            // line 17
            echo "\t\t<tr class=\"brouillons_line\">
\t\t\t<td class=\"brouillons_title\"><a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_consulter", array("id" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
            echo " \">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "new"), "titre"), "html", null, true);
            echo "</a></td>
\t\t\t<td class=\"brouillons_date\">";
            // line 19
            echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getContext($context, "new"), "date"), "d/m/Y"), "html", null, true);
            echo "</td>
\t\t\t<td class=\"brouillons_auteurs\">";
            // line 20
            $context["i"] = 0;
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "new"), "auteur"));
            foreach ($context['_seq'] as $context["_key"] => $context["auteur"]) {
                $context["i"] = ($this->getContext($context, "i") + 1);
                if (($this->getContext($context, "i") > 1)) {
                    echo ", ";
                }
                echo "<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_profile_show"), "html", null, true);
                echo "?pseudo=";
                echo twig_escape_filter($this->env, $this->getContext($context, "auteur"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getContext($context, "auteur"), "html", null, true);
                echo "</a>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['auteur'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            echo "</td>
\t\t\t<td class=\"brouillons_actions\">
\t\t\t\t<a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_editer", array("id" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
            echo "\" >Editer cette news</a> 
\t\t\t\t<a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_supprimer", array("id" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
            echo "\">Supprimer cette news</a>
\t\t\t\t<a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_previsualisation", array("news" => $this->getAttribute($this->getContext($context, "new"), "id"))), "html", null, true);
            echo "\">Prévisualisation</a>
\t\t\t</td>
\t\t</tr>
\t\t
\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 29
            echo "\t\t
\t\t<tr><td colspan=\"4\">Toute les news mises en brouillon ont été publiées </td></tr>
\t
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['new'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 33
        echo "\t\t</table>
\t
\t
";
    }

    public function getTemplateName()
    {
        return "LeimzNewsBundle:Admin:brouillons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
