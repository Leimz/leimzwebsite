<?php

/* LeimzUtilisateurBundle:Admin:lockMembre.html.twig */
class __TwigTemplate_5fdb1420e0c1baaa5aeb9470f50889e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'button' => array($this, 'block_button'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "\t
\t\t\t\t";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.lockMembre.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "
\t\t
";
    }

    // line 18
    public function block_button($context, array $blocks = array())
    {
        // line 19
        echo "\t\t\t";
        $this->env->loadTemplate("LeimzUtilisateurBundle:Admin:lockMembreAjax.html.twig")->display(array_merge($context, array("membre" => $this->getContext($context, "membre"), "form" => $this->getContext($context, "form"), "action" => false)));
        // line 20
        echo "\t\t";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "


\t<h1 class=\"admin_title\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.lockMembre.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</h1>
\t
\t<div class=\"loading\"></div>
\t
\t<div id=\"lockButton\"> 
\t\t";
        // line 18
        $this->displayBlock('button', $context, $blocks);
        // line 21
        echo "\t</div>
<script>\t
\t\t
\t
\t\$(\"#form_bloquage\").submit(function(){ 
    \$(\".loading\").show();
    var membre = \$(\"#lockmembreajax_membre\").val();
    var DATA = 'membre=' + membre + '&action=true';

    \t\$.ajax({
        type: \"POST\",
        url: \"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_lock_membre_ajax"), "html", null, true);
        echo "\",
        data: DATA,
        cache: false,
        success: function(data){
           \$('#lockButton').html(data);
           \$(\".loading\").hide();
  \t\t\t}
  \t\t});
       
    
    return false;
});

</script>

";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Admin:lockMembre.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
