<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_ca374590eed0a656e64c0465704bb774 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
    }

    // line 7
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 8
        echo "
    <h1 class=\"title show_profile_title\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</h1>

    <div class=\"profile_options\">
        <a href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_change_password"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.info.change_password", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a><br/>
        <a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modifier_complement"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.info.edit_profile", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a>
    </div>
    
    <div class=\"profile_avatar\">
    \t";
        // line 17
        if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "avatar", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "avatar") != ""))) {
            // line 18
            echo "    \t\t";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "typeAvatar") == 0)) {
                // line 19
                echo "    \t\t\t<img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "avatar"), "html", null, true);
                echo "\" alt=\"avatar\" title=\"avatar\" width=\"120px\" height= \"120px\"/>
    \t\t";
            } elseif (($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "typeAvatar") == 1)) {
                // line 21
                echo "    \t\t\t<img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("bundles/leimzutilisateur/images/avatars/" . $this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "avatar")) . ".jpg")), "html", null, true);
                echo "\" alt=\"avatar\" title=\"avatar\" width=\"120px\" height= \"120px\" />
    \t\t";
            }
            // line 23
            echo "    \t";
        }
        // line 24
        echo "    </div>
    

    <div class=\"fos_user_user_show\">
        <p>";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "username"), "html", null, true);
        echo "</p>
        <p>";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "email"), "html", null, true);
        echo "</p>
    
    \t<div>
    \t\t";
        // line 32
        if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "birthdate", array(), "any", true, true) && (twig_date_format_filter($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "birthdate"), "d/m/Y") != "30/11/-0001"))) {
            // line 33
            echo "    \t\t\t<p>Date de naissance : ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "birthdate"), "d/m/Y"), "html", null, true);
            echo "</p>
    \t\t";
        }
        // line 35
        echo "    \t\t";
        if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "sexe", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "sexe") != "null"))) {
            // line 36
            echo "    \t\t\t<p>Sexe : ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "sexe") == 0)) {
                echo "Homme";
            } else {
                echo "Femme";
            }
            echo "</p>
    \t\t";
        }
        // line 38
        echo "    \t\t";
        if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "signature", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "signature") != ""))) {
            // line 39
            echo "    \t\t\t<p>Signature : <br/>";
            echo $this->env->getExtension('fm_bbcode')->filter($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "signature"), "default_filter");
            echo "</p>
    \t\t";
        }
        // line 41
        echo "    \t\t";
        if (($this->getAttribute($this->getAttribute($this->getContext($context, "user", true), "complement", array(), "any", false, true), "description", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "description") != ""))) {
            // line 42
            echo "    \t\t\t<p>Description : ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "user"), "complement"), "description"), "html", null, true);
            echo "</p>
    \t\t";
        }
        // line 44
        echo "\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
