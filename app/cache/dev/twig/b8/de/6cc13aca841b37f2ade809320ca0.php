<?php

/* LeimzNewsBundle:Admin:supprimer.html.twig */
class __TwigTemplate_b8de6cc13aca841b37f2ade809320ca0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Suppression d'une news ";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "<h1>Suppression d'une news</h1>
\t";
        // line 7
        if (($this->getContext($context, "confirm") == 0)) {
            // line 8
            echo "\t
\t
\t\t<p>Êtes vous sûr(e) de vouloir supprimer la news \"";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "news"), "titre"), "html", null, true);
            echo "\" ?<br/>
\t\t\t<a href=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_supprimer_confirm", array("id" => $this->getAttribute($this->getContext($context, "news"), "id"), "confirm" => 1)), "html", null, true);
            echo "\">
\t\t\t\t<input type=\"button\" value=\"  Oui  \" />
\t\t\t</a>
\t\t\t<a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_homepage"), "html", null, true);
            echo "\" >
\t\t\t\t<input type=\"button\" value=\"  Non  \" />
\t\t\t</a>
\t\t</p>
\t";
        } else {
            // line 19
            echo "\t\t<p>";
            echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
            echo "</p>
\t";
        }
    }

    public function getTemplateName()
    {
        return "LeimzNewsBundle:Admin:supprimer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
