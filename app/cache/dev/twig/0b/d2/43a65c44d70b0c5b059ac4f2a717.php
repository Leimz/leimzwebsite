<?php

/* LeimzUtilisateurBundle:Admin:displayRoles.html.twig */
class __TwigTemplate_0bd243a65c44d70b0c5b059ac4f2a717 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'Roles' => array($this, 'block_Roles'),
        );
    }

    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('Roles', $context, $blocks);
    }

    public function block_Roles($context, array $blocks = array())
    {
        // line 2
        echo "
\t
\t\t<ul>
\t\t\t";
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "membre"), "roles"));
        foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
            // line 6
            echo "\t\t\t\t<li>";
            echo twig_escape_filter($this->env, $this->getContext($context, "role"), "html", null, true);
            echo ",</li>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 8
        echo "\t\t</ul>
\t

";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Admin:displayRoles.html.twig";
    }

    public function isTraitable()
    {
        return true;
    }
}
