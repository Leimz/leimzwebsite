<?php

/* LeimzNewsBundle:Commentaire:ajouter.html.twig */
class __TwigTemplate_8e833e5104c5c65eec2a554031a0614f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " 
\t";
        // line 4
        if (($this->getContext($context, "edition") == false)) {
            // line 5
            echo "\t\tAjout d'un commentaire 
\t";
        } else {
            // line 7
            echo "\t\tÉdition d'un commentaire
\t";
        }
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "\t<h1 class=\"title\">
\t\t";
        // line 13
        if (($this->getContext($context, "edition") == false)) {
            // line 14
            echo "\t\t\tAjout d'un commentaire 
\t\t";
        } else {
            // line 16
            echo "\t\t\tÉdition d'un commentaire
\t\t";
        }
        // line 18
        echo "\t</h1>

\t<form action=\"\" method=\"post\" class=\"comment_form\" ";
        // line 20
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo " id=\"form_commentaire\">
\t<center>
\t";
        // line 22
        $this->env->loadTemplate("::bbcode.html.twig")->display($context);
        // line 23
        echo "\t
\t\t";
        // line 24
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "contenu"), "Contenu de votre commentaire :");
        echo "
\t\t";
        // line 25
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "contenu"));
        echo "
\t\t
\t\t";
        // line 27
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
\t\t<input type=\"submit\"  value=\"Envoyer le commentaire !\" />
\t\t</center>
\t</form>
<script type=\"text/javascript\">var champTexte = document.getElementById('commentaire_contenu');</script>

";
    }

    public function getTemplateName()
    {
        return "LeimzNewsBundle:Commentaire:ajouter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
