<?php

/* LeimzUtilisateurBundle:Admin:editRolesMembre.html.twig */
class __TwigTemplate_8e79e601a5352c3fcf0df036ebe3efb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'Roles' => array($this, 'block_Roles'),
            'adding' => array($this, 'block_adding'),
            'removing' => array($this, 'block_removing'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "\t\t";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.edit_roles.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "
";
    }

    // line 14
    public function block_Roles($context, array $blocks = array())
    {
        // line 15
        echo "\t\t\t\t";
        $this->env->loadTemplate("LeimzUtilisateurBundle:Admin:displayRoles.html.twig")->display(array_merge($context, array("membre" => $this->getContext($context, "membre"))));
        // line 16
        echo "\t\t\t";
    }

    // line 21
    public function block_adding($context, array $blocks = array())
    {
        // line 22
        echo "\t\t";
        $this->env->loadTemplate("LeimzUtilisateurBundle:Admin:editRoleAddForm.html.twig")->display($context);
        // line 23
        echo "\t";
    }

    // line 28
    public function block_removing($context, array $blocks = array())
    {
        // line 29
        echo "\t\t";
        $this->env->loadTemplate("LeimzUtilisateurBundle:Admin:editRoleRemoveForm.html.twig")->display($context);
        // line 30
        echo "\t";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "\t\t<h1>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.edit_roles.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</h1>
\t\t
\t\t
\t<div id=\"rolesMembreActuels\">
\t<p>";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.edit_roles.rolesZone", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("profile_show_member", array("username" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
        echo "</a> :</p>
\t<div id=\"rolesMembre\">
\t\t\t";
        // line 14
        $this->displayBlock('Roles', $context, $blocks);
        // line 17
        echo "\t</div>
\t</div>

\t<div id=\"addingFormZone\">
\t";
        // line 21
        $this->displayBlock('adding', $context, $blocks);
        // line 24
        echo "\t</div>
\t
\t
\t<div id=\"removingFormZone\">
\t";
        // line 28
        $this->displayBlock('removing', $context, $blocks);
        // line 31
        echo "\t</div>
\t
\t<div id=\"centerZone\">
\t</div>
\t
\t<div class=\"links\">
\t\t       <div><a href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_modifierMembre", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.click.edit_profile_member", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></div>  
\t\t\t";
        // line 38
        if (($this->getAttribute($this->getContext($context, "membre"), "locked") == 0)) {
            // line 39
            echo "\t\t\t\t\t<div><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_lock_membre", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.click.lock_membre", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></div>
\t\t\t";
        } else {
            // line 41
            echo "\t\t\t\t\t<div><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_lock_membre", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.click.unlock_membre", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></div>
\t\t\t";
        }
        // line 43
        echo "\t</div>
\t
\t<script>
\tvar roleZone = \$(\"#rolesMembre\");
\tvar addingZone = \$(\"#addingFormZone\");
\tvar removingZone = \$(\"#removingFormZone\");
\t
\t\$(\"#addRoleForm\").submit(function(){
\t
\t\t\tvar roleToAdd = \$(\"#addroleform_roles\").val();
\t\t\tvar membre = \"";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
        echo "\";
\t\t\tvar DATA = 'membre=' + membre + '&role=' + roleToAdd;
\t\t\t\$.ajax({
        \t\ttype: \"POST\",
        \t\turl: \"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_role_add"), "html", null, true);
        echo "\",
        \t\tdata: DATA,
        \t\tcache: false,
        success: function(data){
           addingZone.html(data);
           DATA2 = 'membre=' + '";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
        echo "';
          \t\t\$.ajax({
        \t\ttype: \"POST\",
        \t\turl: \"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_display_roles"), "html", null, true);
        echo "\",
        \t\tdata: DATA2,
        \t\tcache: false,
        success: function(data){
           roleZone.html(data);
          
  \t\t\t\t\t}
  \t\t\t\t});
  \t\t\t}
  \t\t});
return false;  \t\t
  \t\t});
  \t\t
  \t\t\$(\"#rmRoleForm\").submit(function(){
\t
\t\t\tvar roleToRm = \$(\"#removeroleform_roles\").val();
\t\t\tvar membre = \"";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
        echo "\";
\t\t\tvar DATA = 'membre=' + membre + '&role=' + roleToRm;
\t\t\t\$.ajax({
        \t\ttype: \"POST\",
        \t\turl: \"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_role_remove"), "html", null, true);
        echo "\",
        \t\tdata: DATA,
        \t\tcache: false,
        success: function(data){
           removingZone.html(data);
          \t\tDATA2 = 'membre=' + '";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
        echo "';
          \t\t\$.ajax({
        \t\ttype: \"POST\",
        \t\turl: \"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_display_roles"), "html", null, true);
        echo "\",
        \t\tdata: DATA2,
        \t\tcache: false,
        success: function(data){
           roleZone.html(data);
          
  \t\t\t\t\t}
  \t\t\t\t});
  \t\t\t}
  \t\t});
return false;  \t\t
  \t\t});

\t
\t
\t
\t</script>

";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Admin:editRolesMembre.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
