<?php

/* LeimzNewsBundle:Admin:commentaire.html.twig */
class __TwigTemplate_418a694e45e31b6592bce54d8a214f07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Suppression d'un commentaire";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "\t<h1>Suppression d'un commentaire</h1>
\t";
        // line 7
        if (($this->getContext($context, "confirm") == 0)) {
            // line 8
            echo "\t\t<center><p class=\"comment_suppression\">Êtes vous sûr(e) de vouloir supprimer le commentaire de <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_profile_show"), "html", null, true);
            echo "?pseudo=";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "commentaire"), "getAuteur"), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "commentaire"), "getAuteur"), "html", null, true);
            echo "</a> ?<br/>
\t\t\t<a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_supprimer_commentaire_confirm", array("id" => $this->getAttribute($this->getContext($context, "commentaire"), "id"), "confirm" => 1)), "html", null, true);
            echo "\">
\t\t\t\t<input type=\"button\" value=\"  Oui  \" />
\t\t\t</a>
\t\t\t<a href=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_homepage"), "html", null, true);
            echo "\" >
\t\t\t\t<input type=\"button\" value=\"  Non  \" />
\t\t\t</a>
\t\t</p></center>
\t";
        } else {
            // line 17
            echo "\t\t<p>";
            echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
            echo "</p>
\t";
        }
        // line 19
        echo "

";
    }

    public function getTemplateName()
    {
        return "LeimzNewsBundle:Admin:commentaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
