<?php

/* LeimzUtilisateurBundle:Admin:editRoleAddForm.html.twig */
class __TwigTemplate_21316b23f155149ce2f44089b19f9447 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'adding' => array($this, 'block_adding'),
        );
    }

    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('adding', $context, $blocks);
    }

    public function block_adding($context, array $blocks = array())
    {
        // line 2
        echo "\t<form method=\"POST\" action=\"#\" id=\"addRoleForm\" >
\t\t
\t\t<label for=\"addrole_roles\" class=\"form_label\">";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.edit_roles.form.add.header", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</label>
\t\t ";
        // line 5
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formAdd"), "roles"));
        echo "
\t\t
\t\t
\t\t";
        // line 8
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "formAdd"));
        echo "
\t\t
\t\t<input type=\"submit\" value=\" ";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.edit_roles.form.add.submit", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo " \" />
\t\t
\t</form>
\t";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Admin:editRoleAddForm.html.twig";
    }

    public function isTraitable()
    {
        return true;
    }
}
