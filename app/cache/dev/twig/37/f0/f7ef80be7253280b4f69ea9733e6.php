<?php

/* LeimzPagesBundle:Whatsthat:index.html.twig */
class __TwigTemplate_37f0f7ef80be7253280b4f69ea9733e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Qu'est-ce que c'est ?";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "
    <h1>Leïmz, qu'est-ce que c'est ?</h1>
    
    ";
        // line 9
        if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
            // line 10
            echo "        <p><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("whatsthat_ajouter"), "html", null, true);
            echo "\">Ajouter une catégorie</a></p>
    ";
        }
        // line 12
        echo "        
    ";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "parties"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["partie"]) {
            // line 14
            echo "        ";
            if (($this->getAttribute($this->getContext($context, "partie"), "visible") == 1)) {
                // line 15
                echo "            <h2>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "partie"), "titre"), "html", null, true);
                echo "</h2>
            <p>";
                // line 16
                echo $this->env->getExtension('fm_bbcode')->filter($this->getAttribute($this->getContext($context, "partie"), "contenu"), "default_filter");
                echo "</p>
        ";
            } elseif ((($this->getAttribute($this->getContext($context, "partie"), "visible") == 0) && $this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN"))) {
                // line 18
                echo "            <div class=\"wt_invisible\">
                <p style=\"float:right;\"><a href=\"";
                // line 19
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("whatsthat_editer", array("id" => $this->getAttribute($this->getContext($context, "partie"), "id"))), "html", null, true);
                echo "\"><input style=\"display: inline;\" type=\"button\" value=\"Editer cette partie\"/></a> 
                                        <input type=\"button\" value=\"Monter\" id=\"wtMonter\" />
                                        <input type=\"button\" value=\"Descendre\" id =\"wtDescendre\" />
                <h2>";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "partie"), "titre"), "html", null, true);
                echo "</h2><span>(invisible pour les membres)</span>
                <p>";
                // line 23
                echo $this->env->getExtension('fm_bbcode')->filter($this->getAttribute($this->getContext($context, "partie"), "contenu"), "default_filter");
                echo "</p>
            </div>
        ";
            } else {
                // line 26
                echo "        ";
            }
            // line 27
            echo "    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 28
            echo "        <p>Cette page est en cours de rédaction par l'équipe.</p>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['partie'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 30
        echo "
";
    }

    public function getTemplateName()
    {
        return "LeimzPagesBundle:Whatsthat:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
