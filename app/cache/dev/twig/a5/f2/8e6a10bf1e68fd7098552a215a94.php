<?php

/* LeimzPagesBundle:Default:index.html.twig */
class __TwigTemplate_a5f28e6a10bf1e68fd7098552a215a94 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Hello ";
        echo twig_escape_filter($this->env, $this->getContext($context, "name"), "html", null, true);
        echo "!
";
    }

    public function getTemplateName()
    {
        return "LeimzPagesBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
