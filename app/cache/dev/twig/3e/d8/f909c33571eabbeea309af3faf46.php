<?php

/* LeimzNewsBundle:Admin:ajouter.html.twig */
class __TwigTemplate_3ed8f909c33571eabbeea309af3faf46 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        if (($this->getContext($context, "type") == 1)) {
            // line 4
            echo "\t\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.title.add", array(), "LeimzNewsBundle"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        } else {
            // line 6
            echo "\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.title.edit", array(), "LeimzNewsBundle"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        }
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "
\t\t<h1>";
        // line 11
        if (($this->getContext($context, "type") == 1)) {
            // line 12
            echo "\t\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.title.add", array(), "LeimzNewsBundle"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        } else {
            // line 14
            echo "\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.title.edit", array(), "LeimzNewsBundle"), "html", null, true);
            echo "
\t\t\t\t\t\t";
        }
        // line 15
        echo "</h1>
\t";
        // line 16
        if (($this->getContext($context, "message1") == "1")) {
            // line 17
            echo "\t<p>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.notfound", array(), "LeimzNewsBundle"), "html", null, true);
            echo "</p>
\t";
        }
        // line 19
        echo "\t";
        if (($this->getContext($context, "message1") == "2")) {
            // line 20
            echo "\t<p>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.edit", array(), "LeimzNewsBundle"), "html", null, true);
            echo "</p>
\t";
        }
        // line 22
        echo "\t";
        if (($this->getContext($context, "message1") == "3")) {
            // line 23
            echo "\t<p>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.add", array(), "LeimzNewsBundle"), "html", null, true);
            echo "</p>
\t";
        }
        // line 25
        echo "\t
\t
\t<form action=\"\" method=\"post\" ";
        // line 27
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo " id=\"news_post_form\">
\t
\t\t";
        // line 29
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "titre"));
        echo "
\t\t<label for=\"news_titre\" class=\"addNewsLabel\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.label.title", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</label>
\t\t
\t\t";
        // line 32
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "Image"));
        echo "
\t\t<label for=\"news_image\" class=\"addNewsLabel\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.label.image", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</label>
\t\t
\t\t<div class=\"image_news_explain\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.label.imageExplain", array(), "LeimzNewsBundle"), "html", null, true);
        echo "</div>
\t\t
\t\t\t\t";
        // line 37
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "contenu"), $this->env->getExtension('translator')->trans("admin.ajouter.label.contenu", array(), "LeimzNewsBundle"));
        echo "
\t\t";
        // line 38
        $this->env->loadTemplate("::bbcode.html.twig")->display($context);
        // line 39
        echo "\t\t\t\t";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "contenu"));
        echo "
\t\t\t<div>
\t\t\t\t";
        // line 41
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "brouillon"));
        echo "
\t\t\t\t";
        // line 42
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "brouillon"), "Mettre en brouillon");
        echo "
\t\t\t</div>
\t\t\t\t
\t\t";
        // line 45
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
\t\t<input type=\"submit\" value=\" ";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.ajouter.submit", array(), "LeimzNewsBundle"), "html", null, true);
        echo " \" />
\t</form>
<script type=\"text/javascript\" >
\tvar champTexte = document.getElementById('news_contenu');
\t</script>
";
    }

    public function getTemplateName()
    {
        return "LeimzNewsBundle:Admin:ajouter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
