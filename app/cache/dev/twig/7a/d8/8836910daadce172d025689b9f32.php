<?php

/* LeimzUtilisateurBundle:Admin:modifierMembre.html.twig */
class __TwigTemplate_7ad88836910daadce172d025689b9f32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "\t
\t\t\t\t";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.modifierMembre.title", array("%username%" => $this->getAttribute($this->getContext($context, "membre"), "username")), "LeimzUtilisateurBundle"), "html", null, true);
        echo "
\t\t
";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "


\t<h1 class=\"admin_title\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.modifierMembre.title", array("%username%" => $this->getAttribute($this->getContext($context, "membre"), "username")), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</h1>
\t
\t
\t<form method=\"POST\" action=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_modifierMembre", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
        echo "\" id=\"form_modification\">\t
\t\t
\t\t<div class=\"modificationMembre_item\">
\t\t\t<div class=\"modificationMembre_new\">
\t\t\t\t<label for=\"username\" class=\"modificationMembre_label\" >";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.modifierMembre.form.label.username", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</label>
\t\t\t\t";
        // line 21
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "username"));
        echo "
\t\t\t</div>
\t\t\t<div class=\"modificationMembre_old\" id=\"old_username\">
\t\t\t\t";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.modifierMembre.form.old.username", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t\t
\t\t<div class=\"modificationMembre_item\">
\t\t\t<div class=\"modificationMembre_new\">
\t\t\t\t<label for=\"email\" class=\"modificationMembre_label\" >";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.modifierMembre.form.label.email", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</label>
\t\t\t\t";
        // line 31
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "email"));
        echo "
\t\t\t</div>
\t\t\t<div class=\"modificationMembre_old\" id=\"old_email\">
\t\t\t\t";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.modifierMembre.form.old.email", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "email"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t\t
\t\t
\t\t";
        // line 39
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
\t\t
\t\t<input type=\"submit\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.modifierMembre.form.submit", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "\" />
\t\t
\t</form>
\t<div class=\"links\">
\t\t\t\t";
        // line 45
        if (($this->getAttribute($this->getContext($context, "membre"), "locked") == 0)) {
            // line 46
            echo "\t\t\t\t\t<div><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_lock_membre", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.click.lock_membre", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></div>
\t\t\t\t";
        } else {
            // line 48
            echo "\t\t\t\t\t<div><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_lock_membre", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.click.unlock_membre", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></div>
\t\t\t\t";
        }
        // line 50
        echo "\t\t\t\t\t<div><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_roles_edit", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.click.edit_roles", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></div>
\t</div>
\t<script>
\t\t\t\$(\"#old_username\").hide();
\t\t\t\$(\"#old_email\").hide();
\t\t\t
\t\t\tfunction verifInputs(){
\t\t\t\tvar oldUsername = '";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
        echo "';
\t\t\t\tvar username = \$(\"#profileform_username\").val();\t\t\t
\t\t\t
\t\t\t\tif(oldUsername != username){\$(\"#old_username\").show(500);}else{\$(\"#old_username\").hide(500);}
\t\t\t\t
\t\t\t\tvar oldEmail = '";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "email"), "html", null, true);
        echo "';
\t\t\t\tvar email = \$(\"#profileform_email\").val();\t\t\t
\t\t\t
\t\t\t\tif(oldEmail != email){\$(\"#old_email\").show(500);}else{\$(\"#old_email\").hide(500);}
\t\t\t\t
\t\t\t\t
\t\t\t\tsetTimeout(\"verifInputs()\", 1000);
\t\t\t\treturn false;
\t\t\t}
\t\t\t\t
\t\t\t\tverifInputs();
\t</script>
\t
";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Admin:modifierMembre.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
