<?php

/* LeimzUtilisateurBundle:Admin:lockMembreAjax.html.twig */
class __TwigTemplate_f7c81a8c1299838da3660b66136027ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'button' => array($this, 'block_button'),
        );
    }

    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('button', $context, $blocks);
    }

    public function block_button($context, array $blocks = array())
    {
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "membre"));
        foreach ($context['_seq'] as $context["_key"] => $context["membre1"]) {
            $context["membre"] = $this->getContext($context, "membre1");
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['membre1'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 3
        echo "\t<form action=\"#\" method=\"POST\" id=\"form_bloquage\" >
\t\t<input type=\"hidden\" id=\"lockmembreajax_membre\" name=\"lockmembreajax[id]\" value=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
        echo "\" />
\t\t<input type=\"hidden\" id=\"lock_membreajax_status\" value=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "locked"), "html", null, true);
        echo "\" />
\t\t";
        // line 6
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
\t\t\t<div class=\"lock_membre_resume\">
\t\t<div>
\t\t\t\t";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.lockMembre.verifText1", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "
\t\t\t\t(<a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("profile_show_member", array("username" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
        echo "</a>).
\t\t\t\t";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.lockMembre.verifText2", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "\t\t
\t\t</div>
\t</div>
\t
\t\t<input type=\"submit\" value=\"";
        // line 15
        if (($this->getAttribute($this->getContext($context, "membre"), "locked") == 1)) {
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.lockMembre.submit.unlock", array(), "LeimzUtilisateurBundle"), "html", null, true);
        } else {
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.lockMembre.submit.lock", array(), "LeimzUtilisateurBundle"), "html", null, true);
        }
        echo "\" />
\t\t
\t\t<div>
\t\t";
        // line 18
        if (array_key_exists("done", $context)) {
            // line 19
            echo "\t\t";
            if (($this->getContext($context, "done") == 1)) {
                // line 20
                echo "\t\t\t";
                if (($this->getAttribute($this->getContext($context, "membre"), "locked") == 1)) {
                    // line 21
                    echo "\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.lockMembre.done.locked", array(), "LeimzUtilisateurBundle"), "html", null, true);
                    echo "
\t\t\t";
                } else {
                    // line 23
                    echo "\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.lockMembre.done.unlocked", array(), "LeimzUtilisateurBundle"), "html", null, true);
                    echo "
\t\t\t";
                }
                // line 25
                echo "\t\t";
            }
            // line 26
            echo "\t\t";
        }
        // line 27
        echo "\t\t</div>
\t</form>

";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Admin:lockMembreAjax.html.twig";
    }

    public function isTraitable()
    {
        return true;
    }
}
