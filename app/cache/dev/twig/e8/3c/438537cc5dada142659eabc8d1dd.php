<?php

/* LeimzUtilisateurBundle:Complement:modifier.html.twig */
class __TwigTemplate_e83c438537cc5dada142659eabc8d1dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Complétion de vos informations";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "
\t<h1>Complétion de vos informations</h1>
\t<i class=\"completion_infos\">Tous ces champs sont facultatifs, les informations données ici seront visibles par les autres membres sur votre profil et sur certaines pages du site.</i>
\t<form action=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modifier_complement"), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo " method=\"POST\" id=\"completion_infos_form\" >
\t\t";
        // line 10
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "signature"), "Entrez ici votre signature, elle apparaîtra en dessous de vos messages sur le forum :");
        echo "
\t\t";
        // line 11
        $this->env->loadTemplate("::bbcode.html.twig")->display($context);
        // line 12
        echo "\t\t";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "signature"));
        echo "
\t\t
\t\t<div class=\"sexe_widget\">
\t\t\t";
        // line 15
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "sexe"), "Votre sexe :");
        echo "
\t\t\t";
        // line 16
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "sexe"));
        echo "
\t\t</div>
                
                <div class=\"show_mail\">
                        <div id=\"complement_pub_mail\">";
        // line 20
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "pubEmail"));
        echo "</div>
                        <div>";
        // line 21
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "pubEmail"), "Montrer son email aux autres membres :");
        echo "</div>
\t\t</div>
\t\t
\t\t";
        // line 24
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "description"), "Decrivez vous en quelques lignes :");
        echo "
\t\t";
        // line 25
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "description"));
        echo "
\t\t
\t\t";
        // line 27
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "birthdate"), "Votre de date de naissance (jour-mois-année) :");
        echo "
\t\t";
        // line 28
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "birthdate"));
        echo "
\t\t
\t\t<div id=\"radios_typeAvatar\">
\t\t";
        // line 31
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "typeAvatar"), "Choisissez de quelle manière vous désirez sélectionner votre avatar :");
        echo "
\t\t";
        // line 32
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "typeAvatar"));
        echo "
\t\t</div>
\t\t
\t\t<div id=\"avatar_url\">
\t\t\t";
        // line 36
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "avatar"), "URL de l'image :");
        echo "
\t\t\t";
        // line 37
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "avatar"));
        echo "
\t\t</div>
\t\t
\t\t<div id=\"avatar_liste\">
\t\t\t<p>Liste d'avatars disponibles :</p>
\t\t\t<table class=\"image_avatar\">
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<label for=\"avatar_1\" ><img src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimzutilisateur/images/avatars/1.jpg"), "html", null, true);
        echo "\" width=\"120px\" height=\"120px\"  id=\"image_avatar_1\" class=\"element_avatar\"/></label>
\t\t\t\t\t\t<input type=\"radio\" name=\"liste_avatar\" id=\"avatar_1\" value=\"1\" ></input>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<label for=\"avatar_2\" ><img src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimzutilisateur/images/avatars/2.jpg"), "html", null, true);
        echo "\" width=\"120px\" height=\"120px\"   id=\"image_avatar_2\" class=\"element_avatar\"/></label>
\t\t\t\t\t\t<input type=\"radio\" name=\"liste_avatar\" id=\"avatar_2\" value=\"2\" ></input>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<label for=\"avatar_3\" ><img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimzutilisateur/images/avatars/3.jpg"), "html", null, true);
        echo "\" width=\"120px\" height=\"120px\"  id=\"image_avatar_3\" class=\"element_avatar\" /></label>
\t\t\t\t\t\t<input type=\"radio\" name=\"liste_avatar\" id=\"avatar_3\" value=\"3\" ></input>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</div>
\t
\t\t";
        // line 60
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
\t\t<div></div>
\t\t<input type=\"submit\" value=\"Valider mes informations\" />
\t</form>
\t
\t<script type=\"text/javascript\" >
\t
\t\tvar champTexte = document.getElementById('complement_signature');

\t\t\tif(\$('input[id=\"complement_typeAvatar_0\"]:checked').val())
\t\t\t{
\t\t\t\t\$(\"#avatar_liste\").hide();
\t\t\t\t\$(\"#avatar_url\").show();
\t\t\t}

\t\t\tif(\$('input[id=\"complement_typeAvatar_1\"]:checked').val())
\t\t\t{
\t\t\t\t\$(\"#avatar_url\").hide();
\t\t\t\t\$(\"#avatar_liste\").show();
\t\t\t}

\t\t\t\$('input[name=\"liste_avatar\"]').hide();
                        
                        \$('.element_avatar').attr('style','border : 4px solid #191919;');

\t\t\t
\t\t\t\$('#radios_typeAvatar').change( function(){

\t\t\t\tif(\$('input[id=\"complement_typeAvatar_0\"]:checked').val())
\t\t\t\t{
\t\t\t\t\t\$(\"#avatar_liste\").hide(1000);
\t\t\t\t\t\$(\"#avatar_url\").show(500);

\t\t\t\t\t
\t\t\t\t}

\t\t\t\tif(\$('input[id=\"complement_typeAvatar_1\"]:checked').val())
\t\t\t\t{
\t\t\t\t\t\$(\"#avatar_url\").hide(500);
\t\t\t\t\t\$(\"#avatar_liste\").show(1000);
\t\t\t\t}
\t\t\t\t
\t\t\t});

\t\t\t\$('input[name=\"liste_avatar\"]').change(function() {

\t\t\t\t\$('#complement_avatar').val(\$('input[name=\"liste_avatar\"]:checked').val());
\t\t\t\tvar nombreAvatars = 3;
\t\t\t\tvar i = 0;

\t\t\t\twhile(i<nombreAvatars)
\t\t\t\t{
\t\t\t\t\ti++;
\t\t\t\t\tif(\$('input[id=\"avatar_'+ i +'\"]:checked').val())
\t\t\t\t\t{
\t\t\t\t\t\t\$('.element_avatar').attr('style','border : 4px solid #191919;');
\t\t\t\t\t\t\$('img[id=\"image_avatar_'+ i +'\"]').attr('style','border : 4px solid white; opacity: 0.7;');
\t\t\t\t\t}
\t\t\t\t}
\t\t\t});

\t\t
\t</script>
\t
\t
\t
";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Complement:modifier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
