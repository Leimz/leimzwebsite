<?php

/* FOSUserBundle:ChangePassword:changePassword.html.twig */
class __TwigTemplate_6a84ef713160d1b5a2aa8861db447ec4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change_password.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
    }

    // line 6
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 7
        echo "
    <h1 class=\"change_password_title title\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change_password.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</h1>

    <form action=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_change_password"), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo " method=\"POST\" class=\"fos_user_change_password\">
    
        <div class=\"change_password_item\">
            
            ";
        // line 14
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "current"), $this->env->getExtension('translator')->trans("change_password.current", array(), "LeimzUtilisateurBundle"));
        echo "
            ";
        // line 15
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "current"));
        echo "
            <div class=\"error\">";
        // line 16
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "current"));
        echo "</div>
        
        </div>

        <div class=\"change_password_item\">
            
            ";
        // line 22
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "new"), "first"), $this->env->getExtension('translator')->trans("change_password.new.first", array(), "LeimzUtilisateurBundle"));
        echo "
            ";
        // line 23
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "new"), "first"));
        echo "
            <div class=\"error\">";
        // line 24
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "new"), "first"));
        echo "</div>
        
        </div>

        <div class=\"change_password_item\">
            
            ";
        // line 30
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "new"), "second"), $this->env->getExtension('translator')->trans("change_password.new.second", array(), "LeimzUtilisateurBundle"));
        echo "
            ";
        // line 31
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "new"), "second"));
        echo "
            <div class=\"error\">";
        // line 32
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "new"), "second"));
        echo "</div>
        
        </div>

        ";
        // line 36
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "

        <div>
            <input class=\"change_password_submit\" type=\"submit\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change_password.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
        </div>
    </form>

    <p class=\"link_edit_profile\" >
        <a href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("profile_edit"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.click.edit_profile", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a>
    </p>

";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
