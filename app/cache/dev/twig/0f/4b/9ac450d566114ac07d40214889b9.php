<?php

/* LeimzUtilisateurBundle:Admin:editRoleRemoveForm.html.twig */
class __TwigTemplate_0f4b9ac450d566114ac07d40214889b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'removing' => array($this, 'block_removing'),
        );
    }

    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('removing', $context, $blocks);
    }

    public function block_removing($context, array $blocks = array())
    {
        // line 2
        echo "\t<form method=\"POST\" action=\"#\" id=\"rmRoleForm\" >
\t\t
\t\t<label for=\"removerole_roles\" class=\"form_label\">";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.edit_roles.form.remove.header", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</label>
\t\t ";
        // line 5
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRm"), "roles"));
        echo "
\t\t
\t\t
\t\t";
        // line 8
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "formRm"));
        echo "
\t\t
\t\t<input type=\"submit\" value=\" ";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.edit_roles.form.remove.submit", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo " \" />
\t
\t</form>
\t

\t
";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Admin:editRoleRemoveForm.html.twig";
    }

    public function isTraitable()
    {
        return true;
    }
}
