<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_11643411420c78f2a5b9c4d159249908 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.edit.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 6
        echo "
    <h1 class=\"edit_profile_title title\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.edit.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</div>


    <form action=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_profile_edit"), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo " method=\"POST\" class=\"fos_user_profile_edit\">
            
        <div class=\"edit_profile_item\">

            ";
        // line 14
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "user"), "username"), $this->env->getExtension('translator')->trans("profile.edit.username", array(), "LeimzUtilisateurBundle"));
        echo "
            ";
        // line 15
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "user"), "username"));
        echo "
            ";
        // line 16
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "user"), "username"));
        echo "

        </div>

        <div class=\"edit_profile_item\">

            ";
        // line 22
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "user"), "email"), $this->env->getExtension('translator')->trans("profile.edit.email", array(), "LeimzUtilisateurBundle"));
        echo "
            ";
        // line 23
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "user"), "email"));
        echo "
            ";
        // line 24
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "user"), "email"));
        echo "

        </div>

        <div class=\"edit_profile_item\">

            ";
        // line 30
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "current"), $this->env->getExtension('translator')->trans("profile.edit.current", array(), "LeimzUtilisateurBundle"));
        echo "
            ";
        // line 31
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "current"));
        echo "
            ";
        // line 32
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "current"));
        echo "

        </div>

            ";
        // line 36
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "

        <div class=\"edit_profile_item edit_profile_submit\">
            <input type=\"submit\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.edit.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
        </div>

    </form>

    <p class=\"edit_profile_change_password_link\">
        <a href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_change_password"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.click.change_password", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a>
    </p>

";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
