<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_7bf62ea2756ed961c1851a2a67f2b388 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LeimzUtilisateurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.register.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
    }

    // line 9
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 10
        echo "         <form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_registration_register"), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo " method=\"POST\" class=\"fos_user_registration_register form_registration\">

                <div class=\"registration_item\">

                    ";
        // line 14
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "username"), $this->env->getExtension('translator')->trans("fos_user_registration_form_username", array(), "FOSUserBundle"));
        echo "
                    ";
        // line 15
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "username"));
        echo "
                    <div class=\"errors\">";
        // line 16
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "username"));
        echo "</div>

                </div>
                <div class=\"registration_item\">

                   ";
        // line 21
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "email"), $this->env->getExtension('translator')->trans("fos_user_registration_form_email", array(), "FOSUserBundle"));
        echo "
                   ";
        // line 22
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "email"));
        echo "
                   <div class=\"errors\">";
        // line 23
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "email"));
        echo "</div>

                </div>
                <div class=\"registration_item\">

                    ";
        // line 28
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "plainPassword"), "first"), $this->env->getExtension('translator')->trans("fos_user_registration_form_plainPassword_first", array(), "FOSUserBundle"));
        echo "
                    ";
        // line 29
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "plainPassword"), "first"));
        echo "
                    <div class=\"errors\">";
        // line 30
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "plainPassword"), "first"));
        echo "</div>

                </div>
                <div class=\"registration_item\">

                    ";
        // line 35
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "plainPassword"), "second"), $this->env->getExtension('translator')->trans("fos_user_registration_form_plainPassword_second", array(), "FOSUserBundle"));
        echo "
                    ";
        // line 36
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "plainPassword"), "second"));
        echo "
                    <div class=\"errors\">";
        // line 37
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "plainPassword"), "second"));
        echo "</div>

                </div>

                    ";
        // line 41
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "

                <div class=\"registration_item registration_submit\">
            
                    <input type=\"submit\" value=\" ";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo " \" />
            
                </div>

                <div>
                    <a href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_login"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.click.register.login", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a>
                </div>
        </form>
    ";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "
    <h1 class=\"title registration_title\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.register.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</h1>

    ";
        // line 9
        $this->displayBlock('fos_user_content', $context, $blocks);
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
