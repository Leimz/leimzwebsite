<?php

/* LeimzUtilisateurBundle:Admin:listeMembres.html.twig */
class __TwigTemplate_4a946062104bf0980bd2362ab2cbafa9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'results' => array($this, 'block_results'),
        );
    }

    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('results', $context, $blocks);
    }

    public function block_results($context, array $blocks = array())
    {
        echo " 
 <table class=\"tableResults\">
\t\t<tr class=\"resultsLine titleLine\">
\t\t\t<td>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.table.header.username", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</td>
\t\t\t<td>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.table.header.mail", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</td>
\t\t\t<td>";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.table.header.lastConnection", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</td>
\t\t\t<td>";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.table.header.role", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</td>
\t\t\t<td>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.table.header.statut", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</td>
\t\t\t<td>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.table.header.actions", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</td>
\t\t</tr>
\t\t
\t\t ";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "membres"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["membre"]) {
            // line 13
            echo "\t\t\t
\t\t\t<tr class=\"resultsLine\" >
\t\t\t\t<td><a href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("profile_show_member", array("username" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "username"), "html", null, true);
            echo "</a></td>
\t\t\t\t<td>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "membre"), "email"), "html", null, true);
            echo "</td>
\t\t\t\t<td>";
            // line 17
            echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($this->getContext($context, "membre"), "lastLogin")), "html", null, true);
            echo "</td>
\t\t\t\t<td>";
            // line 18
            if ($this->getAttribute($this->getContext($context, "membre"), "hasRole", array("ROLE_SUPER_ADMIN", ), "method")) {
                // line 19
                echo "\t\t\t\t\t\t";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("roles.admin", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "
\t\t\t\t\t";
            } else {
                // line 21
                echo "\t\t\t\t\t\t";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("roles.member", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "
\t\t\t\t\t";
            }
            // line 23
            echo "\t\t\t\t</td>
\t\t\t\t";
            // line 24
            if (($this->getAttribute($this->getContext($context, "membre"), "locked") == 0)) {
                // line 25
                echo "\t\t\t\t\t<td>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.table.cells.statut.actif", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "</td>
\t\t\t\t";
            } else {
                // line 27
                echo "\t\t\t\t\t<td>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.table.cells.statut.locekd", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "</td>
\t\t\t\t";
            }
            // line 29
            echo "\t\t\t\t<td><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_modifierMembre", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.info.edit_profile_member", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a>  
\t\t\t\t\t\t";
            // line 30
            if (($this->getAttribute($this->getContext($context, "membre"), "locked") == 0)) {
                // line 31
                echo "\t\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_lock_membre", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.info.lock_membre", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "</a>
\t\t\t\t\t\t";
            } else {
                // line 33
                echo "\t\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_lock_membre", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.info.unlock_membre", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "</a>
\t\t\t\t\t\t";
            }
            // line 35
            echo "\t\t\t\t\t<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("leimz_admin_roles_edit", array("membre" => $this->getAttribute($this->getContext($context, "membre"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link.info.edit_roles", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a>
\t\t\t\t\t</td>
\t\t\t</tr>
\t\t\t
\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 39
            echo "\t\t
\t\t
\t\t<tr class=\"resultsLine\" >
\t\t\t<td colspan=\"6\" >";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("admin.listeMembre.table.cells.noUser", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</td>
\t\t</tr>
\t\t
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['membre'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 46
        echo "\t\t
\t</table>
\t
\t";
    }

    public function getTemplateName()
    {
        return "LeimzUtilisateurBundle:Admin:listeMembres.html.twig";
    }

    public function isTraitable()
    {
        return true;
    }
}
