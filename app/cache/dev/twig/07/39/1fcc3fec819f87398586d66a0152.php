<?php

/* ::layout.html.twig */
class __TwigTemplate_07391fcc3fec819f87398586d66a0152 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'menuAdmin' => array($this, 'block_menuAdmin'),
            'menu' => array($this, 'block_menu'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
    
    ";
        // line 6
        echo "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

 
\t
\t<link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />
\t<!--<link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimzadmin/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />-->
\t<!--<link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimzforum/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />-->
\t<link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimzutilisateur/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />
\t
\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />

    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js\" type=\"text/javascript\"></script> 
    </head>
    <body>
    \t<header><a href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">
    \t\t<!-- bannière via css -->
    \t</a></header>

        ";
        // line 25
        $this->displayBlock('menu', $context, $blocks);
        // line 67
        echo "
\t\t<div id=\"corps\">
                    <div id=\"corpsInterieur\">
                    

        ";
        // line 72
        $this->displayBlock('body', $context, $blocks);
        // line 73
        echo "                    </div>
                </div>

\t\t<footer>
\t\t\t<ul>
\t\t\t\t<li><span class=\"footer_staff\">Staff</span>
\t\t\t\t\t<ul class=\"liste_staff\">
\t\t\t\t\t\t<li><a href=\"#\">Développeurs java : Fazega, Chelendil, Kratisto</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Developpeur Web: Blaitox</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">WebDesign: ?</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Graphiste: ?</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Scénariste: Brounch</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Dessinateur: ?</a></li><br/>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li><span class=\"footer_contact\">Contact</span>
\t\t\t\t\t<ul class=\"liste_contact\">
\t\t\t\t\t\t<li><a href=\"#\">Nous contacter</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li><span class=\"footer_aide\">Nous aider</span>
\t\t\t\t\t<ul class=\"liste_aide\">
\t\t\t\t\t\t<li><a href=\"#\">Livre d'or</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Suggestions</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Faire un don</a></li><br/>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li><span class=\"footer_suivre\">Nous suivre</span>
\t\t\t\t\t<ul class=\"liste_suivre\">
\t\t\t\t\t\t<li><a href=\"#\"><div  id=\"footer_facebook\" class=\"image_suivre_footer\"></div></a></li>
\t\t\t\t\t\t<li><a href=\"#\"><div  id=\"footer_twitter\" class=\"image_suivre_footer\"></div></a></li>
\t\t\t\t\t\t<li><a href=\"#\"><div  id=\"footer_rss\" class=\"image_suivre_footer\"></div></a></li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</footer>
                

                <script type=\"text/javascript\">
                    \$('.liste_menu').hide();
                    
                    \$('.liste_menu').mouseleave(function(){
                        \$('.liste_menu').hide();
                        });
                    
                    \$('#menu_gauche').mouseleave(function(){
                        liste = 0;
                        \$('.liste_menu').mouseover(function(){
                            var liste = 1;
                            });
                            if(liste  == 0)
                            {
                                \$('#liste_gauche').hide();
                            }
                            liste = 0;
                        });
                        
                    \$('#menu_gauche').mouseover(function(){
                        \$('#liste_gauche').show(400);
                       });
                       
                   \$('#menu_droite').mouseleave(function(){
                        liste = 0;
                        \$('.liste_menu').mouseover(function(){
                            var liste = 1;
                            });
                            if(liste  == 0)
                            {
                                \$('#liste_droite').hide();
                            }
                            liste = 0;
                        });
                        
                    \$('#menu_droite').mouseover(function(){
                        \$('#liste_droite').show(400);
                       });
                
            
                </script>
            
\t\t<script type=\"text/javascript\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/js/bbcode.js"), "html", null, true);
        echo "\" ></script>
\t\t<script type=\"text/javascript\">

  \t\tvar _gaq = _gaq || [];
  \t\t\t_gaq.push(['_setAccount', 'UA-34677206-1']);
  \t\t\t_gaq.push(['_trackPageview']);

 \t\t\t (function() {
   \t\t\t\t var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   \t\t\t\t ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
   \t\t\t\t var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 \t\t\t\t })();

\t</script>


    </body>
</html>
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
    }

    // line 61
    public function block_menuAdmin($context, array $blocks = array())
    {
    }

    // line 25
    public function block_menu($context, array $blocks = array())
    {
        // line 26
        echo "        <nav>
\t\t\t<nav id=\"menu_gauche\" class=\"menu\">
\t\t\t\t<a href=\"#\" class=\"titre_menu\"><nav>";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.game", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</nav></a>
\t\t\t\t<ul class=\"liste_menu\" id=\"liste_gauche\">
\t\t\t\t\t<li><a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("whatsthat_default"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.leimz", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.comment_jouer", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.download", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.bien_commencer", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.factions", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.perso", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.jobs", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.quetes", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.medias", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t</ul>
\t\t\t</nav>
\t\t\t<nav id=\"menu_droite\" class=\"menu\">
\t\t\t\t<a href=\"#\" class=\"titre_menu\"><nav>";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.communaute", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</nav></a>
\t\t\t\t<ul class=\"liste_menu\" id=\"liste_droite\">
\t\t\t\t\t";
        // line 44
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 45
            echo "\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("profile_show"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.authenticated.profil", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></li>
                                                
                                                ";
            // line 47
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 48
                echo "                                                    <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_panneau"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.authenticated.admin", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "</a></li>
                                                ";
            }
            // line 50
            echo "
\t\t\t\t\t\t<li><a href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_logout"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.authenticated.logout", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t";
        } else {
            // line 53
            echo "\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_login"), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.anonymous.connexion", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t\t<li class=\"menu_pair\"><a href=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("register"), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.anonymous.register", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t";
        }
        // line 56
        echo "\t\t\t\t\t<li class=\"menu_pair\"><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.news", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li class=\"menu_impair\"><a href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.forum", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li class=\"menu_pair\"><a href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.ladder", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li class=\"menu_impair\"><a href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.assistance", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li class=\"menu_pair\"><a href=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.faq", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t";
        // line 61
        $this->displayBlock('menuAdmin', $context, $blocks);
        // line 62
        echo "\t\t\t\t</ul>
\t\t\t</nav>
\t\t\t<nav id=\"jouer\"><a href=\"#\"></a></nav>
\t\t</nav>
\t\t";
    }

    // line 72
    public function block_body($context, array $blocks = array())
    {
        echo "<p>Aucun contenu trouvé.</p>";
    }

    public function getTemplateName()
    {
        return "::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
