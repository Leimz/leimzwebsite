<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;


/**
 * appdevUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRouteNames = array(
       '_wdt' => true,
       '_profiler_search' => true,
       '_profiler_purge' => true,
       '_profiler_import' => true,
       '_profiler_export' => true,
       '_profiler_search_results' => true,
       '_profiler' => true,
       '_configurator_home' => true,
       '_configurator_step' => true,
       '_configurator_final' => true,
       'whatsthat_default' => true,
       'whatsthat_ajouter' => true,
       'whatsthat_editer' => true,
       'whatsthat_voirInvi' => true,
       'whatsthat_changerOrdre' => true,
       'admin_panneau' => true,
       'homepage' => true,
       'news_homepage' => true,
       'news_consulter' => true,
       'news_commenter' => true,
       'news_editer_commentaire' => true,
       'news_editer' => true,
       'news_via_tags' => true,
       'admin_news_supprimer' => true,
       'admin_news_supprimer_confirm' => true,
       'admin_news_ajouter' => true,
       'admin_news_editer' => true,
       'admin_news_verrouiller' => true,
       'admin_news_verrouiller_confirm' => true,
       'admin_news_supprimer_commentaire' => true,
       'admin_news_supprimer_commentaire_confirm' => true,
       'admin_news_brouillons' => true,
       'todo' => true,
       'admin_news_previsualisation' => true,
       'admin_news_voir_supprimees' => true,
       'user_login' => true,
       'user_login_check' => true,
       'user_logout' => true,
       'profile_show' => true,
       'profile_edit' => true,
       'profile_show_member' => true,
       'register' => true,
       'fos_user_security_login' => true,
       'fos_user_security_check' => true,
       'fos_user_security_logout' => true,
       'fos_user_profile_show' => true,
       'fos_user_profile_edit' => true,
       'fos_user_registration_register' => true,
       'fos_user_registration_check_email' => true,
       'fos_user_registration_confirm' => true,
       'fos_user_registration_confirmed' => true,
       'fos_user_resetting_request' => true,
       'fos_user_resetting_send_email' => true,
       'fos_user_resetting_check_email' => true,
       'fos_user_resetting_reset' => true,
       'fos_user_change_password' => true,
       'fos_user_group_list' => true,
       'fos_user_group_new' => true,
       'fos_user_group_show' => true,
       'fos_user_group_edit' => true,
       'fos_user_group_delete' => true,
       'admin_voir_membres' => true,
       'leimz_utilisateur_rechercher' => true,
       'admin_voir_membres_locked' => true,
       'leimz_admin_lock_membre' => true,
       'leimz_admin_lock_membre_ajax' => true,
       'leimz_admin_modifierMembre' => true,
       'leimz_admin_roles_edit' => true,
       'leimz_admin_role_add' => true,
       'leimz_admin_role_remove' => true,
       'leimz_display_roles' => true,
       'modifier_complement' => true,
       'console' => true,
       'console_exec' => true,
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function generate($name, $parameters = array(), $absolute = false)
    {
        if (!isset(self::$declaredRouteNames[$name])) {
            throw new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }

        $escapedName = str_replace('.', '__', $name);

        list($variables, $defaults, $requirements, $tokens) = $this->{'get'.$escapedName.'RouteInfo'}();

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $absolute);
    }

    private function get_wdtRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_wdt',  ),));
    }

    private function get_profiler_searchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/search',  ),));
    }

    private function get_profiler_purgeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/purge',  ),));
    }

    private function get_profiler_importRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/import',  ),));
    }

    private function get_profiler_exportRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '.txt',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/\\.]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler/export',  ),));
    }

    private function get_profiler_search_resultsRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/search/results',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_profilerRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_configurator_homeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/',  ),));
    }

    private function get_configurator_stepRouteInfo()
    {
        return array(array (  0 => 'index',), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'index',  ),  1 =>   array (    0 => 'text',    1 => '/_configurator/step',  ),));
    }

    private function get_configurator_finalRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/final',  ),));
    }

    private function getwhatsthat_defaultRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\PagesBundle\\Controller\\WhatsthatController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/qu-est-ce-que-c-est',  ),));
    }

    private function getwhatsthat_ajouterRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\PagesBundle\\Controller\\WhatsthatController::ajouterAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/qu-est-ce-que-c-est/ajouter',  ),));
    }

    private function getwhatsthat_editerRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Leimz\\PagesBundle\\Controller\\WhatsthatController::ajouterAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/admin/qu-est-ce-que-c-est/editer',  ),));
    }

    private function getwhatsthat_voirInviRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\PagesBundle\\Controller\\WhatsthatController::invisiblesAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/qu-est-ce-que-c-est/brouillons',  ),));
    }

    private function getwhatsthat_changerOrdreRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\PagesBundle\\Controller\\WhatsthatController::changerOrdreAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/qu-est-ce-que-c-est/ordre',  ),));
    }

    private function getadmin_panneauRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\AdminBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/',  ),));
    }

    private function gethomepageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/',  ),));
    }

    private function getnews_homepageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/news',  ),));
    }

    private function getnews_consulterRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\NewsController::consulterAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/news',  ),));
    }

    private function getnews_commenterRouteInfo()
    {
        return array(array (  0 => 'news',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\CommentaireController::ajouterAction',), array (  'news' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'news',  ),  1 =>   array (    0 => 'text',    1 => '/news-commenter',  ),));
    }

    private function getnews_editer_commentaireRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\CommentaireController::ajouterAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/news-editer-commentaire',  ),));
    }

    private function getnews_editerRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\CommentaireController::ajouterAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/news-editer',  ),));
    }

    private function getnews_via_tagsRouteInfo()
    {
        return array(array (  0 => 'tag',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\NewsController::parTagAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'tag',  ),  1 =>   array (    0 => 'text',    1 => '/news',  ),));
    }

    private function getadmin_news_supprimerRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::supprimerAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/admin/news/supprimer',  ),));
    }

    private function getadmin_news_supprimer_confirmRouteInfo()
    {
        return array(array (  0 => 'id',  1 => 'confirm',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::supprimerAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'confirm',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/news/supprimer',  ),));
    }

    private function getadmin_news_ajouterRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::ajouterAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/news/ajouter',  ),));
    }

    private function getadmin_news_editerRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::ajouterAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/admin/news/editer',  ),));
    }

    private function getadmin_news_verrouillerRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::verrouillerAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/admin/news/verrouiller',  ),));
    }

    private function getadmin_news_verrouiller_confirmRouteInfo()
    {
        return array(array (  0 => 'id',  1 => 'confirm',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::verrouillerAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'confirm',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/news/verrouiller',  ),));
    }

    private function getadmin_news_supprimer_commentaireRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::supprimerCommentaireAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/admin/news/supprimer-commentaire',  ),));
    }

    private function getadmin_news_supprimer_commentaire_confirmRouteInfo()
    {
        return array(array (  0 => 'id',  1 => 'confirm',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::supprimerCommentaireAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'confirm',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/news/supprimer-commentaire',  ),));
    }

    private function getadmin_news_brouillonsRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::brouillonsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/news/brouillons',  ),));
    }

    private function gettodoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'LeimzNewsBundle:ToDo:dashBoard',), array (), array (  0 =>   array (    0 => 'text',    1 => '/todo',  ),));
    }

    private function getadmin_news_previsualisationRouteInfo()
    {
        return array(array (  0 => 'news',), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::previsualisationAction',), array (  'news' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'news',  ),  1 =>   array (    0 => 'text',    1 => '/admin/news/previsualisation',  ),));
    }

    private function getadmin_news_voir_supprimeesRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::voirSupprimeesAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/news/supprimees',  ),));
    }

    private function getuser_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/connexion',  ),));
    }

    private function getuser_login_checkRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/check-connexion',  ),));
    }

    private function getuser_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/deconnexion',  ),));
    }

    private function getprofile_showRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/profil',  ),));
    }

    private function getprofile_editRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/modifier-profil',  ),));
    }

    private function getprofile_show_memberRouteInfo()
    {
        return array(array (  0 => 'username',), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::showMemberAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'username',  ),  1 =>   array (    0 => 'text',    1 => '/voir-profil',  ),));
    }

    private function getregisterRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/inscription',  ),));
    }

    private function getfos_user_security_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login',  ),));
    }

    private function getfos_user_security_checkRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login_check',  ),));
    }

    private function getfos_user_security_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/logout',  ),));
    }

    private function getfos_user_profile_showRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::showAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/profile/',  ),));
    }

    private function getfos_user_profile_editRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/profile/edit',  ),));
    }

    private function getfos_user_registration_registerRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/register/',  ),));
    }

    private function getfos_user_registration_check_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/register/check-email',  ),));
    }

    private function getfos_user_registration_confirmRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/register/confirm',  ),));
    }

    private function getfos_user_registration_confirmedRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/register/confirmed',  ),));
    }

    private function getfos_user_resetting_requestRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/request',  ),));
    }

    private function getfos_user_resetting_send_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',), array (  '_method' => 'POST',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/send-email',  ),));
    }

    private function getfos_user_resetting_check_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/check-email',  ),));
    }

    private function getfos_user_resetting_resetRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',), array (  '_method' => 'GET|POST',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/resetting/reset',  ),));
    }

    private function getfos_user_change_passwordRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',), array (  '_method' => 'GET|POST',), array (  0 =>   array (    0 => 'text',    1 => '/change-password/change-password',  ),));
    }

    private function getfos_user_group_listRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::listAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/group/list',  ),));
    }

    private function getfos_user_group_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/group/new',  ),));
    }

    private function getfos_user_group_showRouteInfo()
    {
        return array(array (  0 => 'groupname',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::showAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'groupname',  ),  1 =>   array (    0 => 'text',    1 => '/group',  ),));
    }

    private function getfos_user_group_editRouteInfo()
    {
        return array(array (  0 => 'groupname',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'groupname',  ),  2 =>   array (    0 => 'text',    1 => '/group',  ),));
    }

    private function getfos_user_group_deleteRouteInfo()
    {
        return array(array (  0 => 'groupname',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::deleteAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'groupname',  ),  2 =>   array (    0 => 'text',    1 => '/group',  ),));
    }

    private function getadmin_voir_membresRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::ListerMembresAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/listeMembres',  ),));
    }

    private function getleimz_utilisateur_rechercherRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::rechercherMembreAction',), array (  '_method' => 'POST',), array (  0 =>   array (    0 => 'text',    1 => '/admin/ajaxListe',  ),));
    }

    private function getadmin_voir_membres_lockedRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::ListerMembresLockedAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/listeMembresLocked',  ),));
    }

    private function getleimz_admin_lock_membreRouteInfo()
    {
        return array(array (  0 => 'membre',), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::lockMembreAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'membre',  ),  1 =>   array (    0 => 'text',    1 => '/admin/lock',  ),));
    }

    private function getleimz_admin_lock_membre_ajaxRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::lockMembreAjaxAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/lockMembreAjax',  ),));
    }

    private function getleimz_admin_modifierMembreRouteInfo()
    {
        return array(array (  0 => 'membre',), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::modifierMembreAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'membre',  ),  1 =>   array (    0 => 'text',    1 => '/admin/modifier',  ),));
    }

    private function getleimz_admin_roles_editRouteInfo()
    {
        return array(array (  0 => 'membre',), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::editRolesAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'membre',  ),  1 =>   array (    0 => 'text',    1 => '/admin/roles',  ),));
    }

    private function getleimz_admin_role_addRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::addRoleAjaxAction',), array (  '_method' => 'POST',), array (  0 =>   array (    0 => 'text',    1 => '/admin/role/add',  ),));
    }

    private function getleimz_admin_role_removeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::removeRoleAjaxAction',), array (  '_method' => 'POST',), array (  0 =>   array (    0 => 'text',    1 => '/admin/role/rm',  ),));
    }

    private function getleimz_display_rolesRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::displayRolesAction',), array (  '_method' => 'POST',), array (  0 =>   array (    0 => 'text',    1 => '/admin/display-roles',  ),));
    }

    private function getmodifier_complementRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ComplementController::modifierComplementAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/modifier-profil/',  ),));
    }

    private function getconsoleRouteInfo()
    {
        return array(array (), array (  '_controller' => 'CoreSphere\\ConsoleBundle\\Controller\\ConsoleController::indexAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/console/',  ),));
    }

    private function getconsole_execRouteInfo()
    {
        return array(array (  0 => '_format',), array (  '_controller' => 'CoreSphere\\ConsoleBundle\\Controller\\ConsoleController::execAction',  '_format' => 'json',), array (  '_method' => 'POST',  '_format' => 'json|html',), array (  0 =>   array (    0 => 'variable',    1 => '.',    2 => 'json|html',    3 => '_format',  ),  1 =>   array (    0 => 'text',    1 => '/console',  ),));
    }
}
