<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appprodUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appprodUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // admin_panneau
        if (rtrim($pathinfo, '/') === '/admin') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_panneau');
            }
            return array (  '_controller' => 'Leimz\\AdminBundle\\Controller\\DefaultController::indexAction',  '_route' => 'admin_panneau',);
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }
            return array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        // news_homepage
        if ($pathinfo === '/news') {
            return array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\DefaultController::indexAction',  '_route' => 'news_homepage',);
        }

        // news_consulter
        if (0 === strpos($pathinfo, '/news') && preg_match('#^/news/(?P<id>\\d+)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\NewsController::consulterAction',)), array('_route' => 'news_consulter'));
        }

        // news_commenter
        if (0 === strpos($pathinfo, '/news-commenter') && preg_match('#^/news\\-commenter/(?P<news>\\d+)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\CommentaireController::ajouterAction',)), array('_route' => 'news_commenter'));
        }

        // news_editer_commentaire
        if (0 === strpos($pathinfo, '/news-editer-commentaire') && preg_match('#^/news\\-editer\\-commentaire/(?P<id>\\d+)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\CommentaireController::ajouterAction',)), array('_route' => 'news_editer_commentaire'));
        }

        // news_editer
        if (0 === strpos($pathinfo, '/news-editer') && preg_match('#^/news\\-editer/(?P<id>\\d+)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\CommentaireController::ajouterAction',)), array('_route' => 'news_editer'));
        }

        // news_via_tags
        if (0 === strpos($pathinfo, '/news') && preg_match('#^/news/(?P<tag>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\NewsController::parTagAction',)), array('_route' => 'news_via_tags'));
        }

        // admin_news_supprimer
        if (0 === strpos($pathinfo, '/admin/news/supprimer') && preg_match('#^/admin/news/supprimer/(?P<id>\\d+)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::supprimerAction',)), array('_route' => 'admin_news_supprimer'));
        }

        // admin_news_supprimer_confirm
        if (0 === strpos($pathinfo, '/admin/news/supprimer') && preg_match('#^/admin/news/supprimer/(?P<id>\\d+)/(?P<confirm>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::supprimerAction',)), array('_route' => 'admin_news_supprimer_confirm'));
        }

        // admin_news_ajouter
        if ($pathinfo === '/admin/news/ajouter') {
            return array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::ajouterAction',  '_route' => 'admin_news_ajouter',);
        }

        // admin_news_editer
        if (0 === strpos($pathinfo, '/admin/news/editer') && preg_match('#^/admin/news/editer/(?P<id>\\d+)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::ajouterAction',)), array('_route' => 'admin_news_editer'));
        }

        // admin_news_verrouiller
        if (0 === strpos($pathinfo, '/admin/news/verrouiller') && preg_match('#^/admin/news/verrouiller/(?P<id>\\d+)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::verrouillerAction',)), array('_route' => 'admin_news_verrouiller'));
        }

        // admin_news_verrouiller_confirm
        if (0 === strpos($pathinfo, '/admin/news/verrouiller') && preg_match('#^/admin/news/verrouiller/(?P<id>\\d+)/(?P<confirm>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::verrouillerAction',)), array('_route' => 'admin_news_verrouiller_confirm'));
        }

        // admin_news_supprimer_commentaire
        if (0 === strpos($pathinfo, '/admin/news/supprimer-commentaire') && preg_match('#^/admin/news/supprimer\\-commentaire/(?P<id>\\d+)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::supprimerCommentaireAction',)), array('_route' => 'admin_news_supprimer_commentaire'));
        }

        // admin_news_supprimer_commentaire_confirm
        if (0 === strpos($pathinfo, '/admin/news/supprimer-commentaire') && preg_match('#^/admin/news/supprimer\\-commentaire/(?P<id>\\d+)/(?P<confirm>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::supprimerCommentaireAction',)), array('_route' => 'admin_news_supprimer_commentaire_confirm'));
        }

        // admin_news_brouillons
        if ($pathinfo === '/admin/news/brouillons') {
            return array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::brouillonsAction',  '_route' => 'admin_news_brouillons',);
        }

        // todo
        if ($pathinfo === '/todo') {
            return array (  '_controller' => 'LeimzNewsBundle:ToDo:dashBoard',  '_route' => 'todo',);
        }

        // admin_news_previsualisation
        if (0 === strpos($pathinfo, '/admin/news/previsualisation') && preg_match('#^/admin/news/previsualisation/(?P<news>\\d+)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::previsualisationAction',)), array('_route' => 'admin_news_previsualisation'));
        }

        // admin_news_voir_supprimees
        if ($pathinfo === '/admin/news/supprimees') {
            return array (  '_controller' => 'Leimz\\NewsBundle\\Controller\\AdminController::voirSupprimeesAction',  '_route' => 'admin_news_voir_supprimees',);
        }

        // user_login
        if ($pathinfo === '/connexion') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'user_login',);
        }

        // user_login_check
        if ($pathinfo === '/check-connexion') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'user_login_check',);
        }

        // user_logout
        if ($pathinfo === '/deconnexion') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'user_logout',);
        }

        // profile_show
        if ($pathinfo === '/profil') {
            return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::showAction',  '_route' => 'profile_show',);
        }

        // profile_edit
        if ($pathinfo === '/modifier-profil') {
            return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::editAction',  '_route' => 'profile_edit',);
        }

        // profile_show_member
        if (0 === strpos($pathinfo, '/voir-profil') && preg_match('#^/voir\\-profil/(?P<username>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::showMemberAction',)), array('_route' => 'profile_show_member'));
        }

        // register
        if ($pathinfo === '/inscription') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'register',);
        }

        // fos_user_security_login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
        }

        // fos_user_security_check
        if ($pathinfo === '/login_check') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
        }

        // fos_user_security_logout
        if ($pathinfo === '/logout') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }
                return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }

        }

        if (0 === strpos($pathinfo, '/register')) {
            // fos_user_registration_register
            if (rtrim($pathinfo, '/') === '/register') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
            }

            // fos_user_registration_check_email
            if ($pathinfo === '/register/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_check_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
            }
            not_fos_user_registration_check_email:

            // fos_user_registration_confirm
            if (0 === strpos($pathinfo, '/register/confirm') && preg_match('#^/register/confirm/(?P<token>[^/]+?)$#xs', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirm;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',)), array('_route' => 'fos_user_registration_confirm'));
            }
            not_fos_user_registration_confirm:

            // fos_user_registration_confirmed
            if ($pathinfo === '/register/confirmed') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirmed;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
            }
            not_fos_user_registration_confirmed:

        }

        if (0 === strpos($pathinfo, '/resetting')) {
            // fos_user_resetting_request
            if ($pathinfo === '/resetting/request') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_request;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_send_email
            if ($pathinfo === '/resetting/send-email') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fos_user_resetting_send_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ($pathinfo === '/resetting/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_check_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
            }
            not_fos_user_resetting_check_email:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]+?)$#xs', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_resetting_reset;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',)), array('_route' => 'fos_user_resetting_reset'));
            }
            not_fos_user_resetting_reset:

        }

        // fos_user_change_password
        if ($pathinfo === '/change-password/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        if (0 === strpos($pathinfo, '/group')) {
            // fos_user_group_list
            if ($pathinfo === '/group/list') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_group_list;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::listAction',  '_route' => 'fos_user_group_list',);
            }
            not_fos_user_group_list:

            // fos_user_group_new
            if ($pathinfo === '/group/new') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::newAction',  '_route' => 'fos_user_group_new',);
            }

            // fos_user_group_show
            if (preg_match('#^/group/(?P<groupname>[^/]+?)$#xs', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_group_show;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::showAction',)), array('_route' => 'fos_user_group_show'));
            }
            not_fos_user_group_show:

            // fos_user_group_edit
            if (preg_match('#^/group/(?P<groupname>[^/]+?)/edit$#xs', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::editAction',)), array('_route' => 'fos_user_group_edit'));
            }

            // fos_user_group_delete
            if (preg_match('#^/group/(?P<groupname>[^/]+?)/delete$#xs', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_group_delete;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::deleteAction',)), array('_route' => 'fos_user_group_delete'));
            }
            not_fos_user_group_delete:

        }

        // admin_voir_membres
        if ($pathinfo === '/admin/listeMembres') {
            return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::ListerMembresAction',  '_route' => 'admin_voir_membres',);
        }

        // leimz_utilisateur_rechercher
        if ($pathinfo === '/admin/ajaxListe') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_leimz_utilisateur_rechercher;
            }
            return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::rechercherMembreAction',  '_route' => 'leimz_utilisateur_rechercher',);
        }
        not_leimz_utilisateur_rechercher:

        // admin_voir_membres_locked
        if ($pathinfo === '/admin/listeMembresLocked') {
            return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::ListerMembresLockedAction',  '_route' => 'admin_voir_membres_locked',);
        }

        // leimz_admin_lock_membre
        if (0 === strpos($pathinfo, '/admin/lock') && preg_match('#^/admin/lock/(?P<membre>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::lockMembreAction',)), array('_route' => 'leimz_admin_lock_membre'));
        }

        // leimz_admin_lock_membre_ajax
        if ($pathinfo === '/admin/lockMembreAjax') {
            return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::lockMembreAjaxAction',  '_route' => 'leimz_admin_lock_membre_ajax',);
        }

        // leimz_admin_modifierMembre
        if (0 === strpos($pathinfo, '/admin/modifier') && preg_match('#^/admin/modifier/(?P<membre>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::modifierMembreAction',)), array('_route' => 'leimz_admin_modifierMembre'));
        }

        // leimz_admin_roles_edit
        if (0 === strpos($pathinfo, '/admin/roles') && preg_match('#^/admin/roles/(?P<membre>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::editRolesAction',)), array('_route' => 'leimz_admin_roles_edit'));
        }

        // leimz_admin_role_add
        if ($pathinfo === '/admin/role/add') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_leimz_admin_role_add;
            }
            return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::addRoleAjaxAction',  '_route' => 'leimz_admin_role_add',);
        }
        not_leimz_admin_role_add:

        // leimz_admin_role_remove
        if ($pathinfo === '/admin/role/rm') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_leimz_admin_role_remove;
            }
            return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::removeRoleAjaxAction',  '_route' => 'leimz_admin_role_remove',);
        }
        not_leimz_admin_role_remove:

        // leimz_display_roles
        if (rtrim($pathinfo, '/') === '/admin/display-roles') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_leimz_display_roles;
            }
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'leimz_display_roles');
            }
            return array (  '_controller' => 'Leimz\\UtilisateurBundle\\Controller\\AdminController::displayRolesAction',  '_route' => 'leimz_display_roles',);
        }
        not_leimz_display_roles:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
