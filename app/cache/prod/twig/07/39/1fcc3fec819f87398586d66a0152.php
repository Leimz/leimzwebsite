<?php

/* ::layout.html.twig */
class __TwigTemplate_07391fcc3fec819f87398586d66a0152 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'menuAdmin' => array($this, 'block_menuAdmin'),
            'menu' => array($this, 'block_menu'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return false;
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
    
    ";
        // line 6
        echo "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        ";
        // line 17
        echo "\t
\t<link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />
\t<!--<link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimzadmin/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />-->
\t<!--<link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimzforum/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />-->
\t<link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimzutilisateur/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />
\t
\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />

    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js\" type=\"text/javascript\"></script> 
    </head>
    <body>
    \t<header><a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">
    \t\t<!-- banni�re via css -->
    \t</a></header>

        ";
        // line 32
        $this->displayBlock('menu', $context, $blocks);
        // line 74
        echo "
\t\t<div id=\"corps\">
                    <div id=\"corpsInterieur\">
                    

        ";
        // line 79
        $this->displayBlock('body', $context, $blocks);
        // line 80
        echo "                    </div>
                </div>

\t\t<footer>
\t\t\t<ul>
\t\t\t\t<li><span class=\"footer_staff\">Staff</span>
\t\t\t\t\t<ul class=\"liste_staff\">
\t\t\t\t\t\t<li><a href=\"#\">Developpeurs Java: FaZeGa / Grunty</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Developpeur Web: Blaitox</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">WebDesign: Arkenzo GFX</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Graphiste: La Satyre</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Developpement Serveur: Kratisto</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Dessinateur: Cleon</a></li><br/>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li><span class=\"footer_contact\">Contact</span>
\t\t\t\t\t<ul class=\"liste_contact\">
\t\t\t\t\t\t<li><a href=\"#\">Nous contacter</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li><span class=\"footer_aide\">Nous aider</span>
\t\t\t\t\t<ul class=\"liste_aide\">
\t\t\t\t\t\t<li><a href=\"#\">Livre d'or</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Suggestions</a></li><br/>
\t\t\t\t\t\t<li><a href=\"#\">Faire un don</a></li><br/>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li><span class=\"footer_suivre\">Nous suivre</span>
\t\t\t\t\t<ul class=\"liste_suivre\">
\t\t\t\t\t\t<li><a href=\"#\"><div  id=\"footer_facebook\" class=\"image_suivre_footer\"></div></a></li>
\t\t\t\t\t\t<li><a href=\"#\"><div  id=\"footer_twitter\" class=\"image_suivre_footer\"></div></a></li>
\t\t\t\t\t\t<li><a href=\"#\"><div  id=\"footer_rss\" class=\"image_suivre_footer\"></div></a></li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</footer>

";
        // line 128
        echo "\t
\t\t<script type=\"text/javascript\" src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/js/bbcode.js"), "html", null, true);
        echo "\" ></script>


    </body>
</html>
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.title", array(), "LeimzUtilisateurBundle"), "html", null, true);
    }

    // line 68
    public function block_menuAdmin($context, array $blocks = array())
    {
    }

    // line 32
    public function block_menu($context, array $blocks = array())
    {
        // line 33
        echo "        <nav>
\t\t\t<nav id=\"menu_gauche\" class=\"menu\">
\t\t\t\t<a href=\"#\" class=\"titre_menu\"><nav>";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.game", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</nav></a>
\t\t\t\t<ul>
\t\t\t\t\t<li><a href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.leimz", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.comment_jouer", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.download", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.bien_commencer", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.factions", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.perso", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.jobs", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.quetes", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li><a href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.medias", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t</ul>
\t\t\t</nav>
\t\t\t<nav id=\"menu_droite\" class=\"menu\">
\t\t\t\t<a href=\"#\" class=\"titre_menu\"><nav>";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.communaute", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</nav></a>
\t\t\t\t<ul>
\t\t\t\t\t";
        // line 51
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 52
            echo "\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("profile_show"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.authenticated.profil", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></li>
                                                
                                                ";
            // line 54
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 55
                echo "                                                    <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_panneau"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.authenticated.admin", array(), "LeimzUtilisateurBundle"), "html", null, true);
                echo "</a></li>
                                                ";
            }
            // line 57
            echo "
\t\t\t\t\t\t<li><a href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_logout"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.authenticated.logout", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t";
        } else {
            // line 60
            echo "\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_login"), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.anonymous.connexion", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t\t<li class=\"menu_pair\"><a href=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("register"), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.anonymous.register", array(), "LeimzUtilisateurBundle"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t";
        }
        // line 63
        echo "\t\t\t\t\t<li class=\"menu_pair\"><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.news", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li class=\"menu_impair\"><a href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.forum", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li class=\"menu_pair\"><a href=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.ladder", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li class=\"menu_impair\"><a href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.assistance", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t<li class=\"menu_pair\"><a href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("homepage"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("main.layout.menu.faq", array(), "LeimzUtilisateurBundle"), "html", null, true);
        echo "</a></li>
\t\t\t\t\t";
        // line 68
        $this->displayBlock('menuAdmin', $context, $blocks);
        // line 69
        echo "\t\t\t\t</ul>
\t\t\t</nav>
\t\t\t<nav id=\"jouer\"><a href=\"#\"></a></nav>
\t\t</nav>
\t\t";
    }

    // line 79
    public function block_body($context, array $blocks = array())
    {
        echo "<p>Aucun contenu trouvé.</p>";
    }

    public function getTemplateName()
    {
        return "::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
