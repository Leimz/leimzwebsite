<?php

/* LeimzNewsBundle:Default:index.html.twig */
class __TwigTemplate_60672d041af11bb3b1d22d4c6aaa7f95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Page d'accueil ";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        echo "<h1 class=\"titre_page\">Bienvenue sur le site du jeu Leïmz !</h1>



\t\t";
        // line 9
        if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
        if ((twig_length_filter($this->env, $_pagination_) > 0)) {
            // line 10
            echo "\t\t\t\t
\t\t\t\t";
            // line 11
            if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
                // line 12
                echo "\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_ajouter"), "html", null, true);
                echo "\" >Ajouter une news</a><br/>
\t\t\t\t\t
\t\t\t\t";
            }
            // line 15
            echo "\t\t\t\t
\t\t\t<table class=\"news_table\">

\t\t\t";
            // line 18
            if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_pagination_);
            foreach ($context['_seq'] as $context["_key"] => $context["new"]) {
                // line 19
                echo "\t\t\t\t<tr class=\"news_ligne\">
\t\t\t\t\t<td class=\"news_colonne\">
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<a class=\"news_titre\" href=\"";
                // line 22
                if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_consulter", array("id" => $this->getAttribute($_new_, "id"))), "html", null, true);
                echo "\" >
\t\t\t\t\t\t\t";
                // line 23
                if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                if (($this->getAttribute($_new_, "image") != "default")) {
                    // line 24
                    echo "\t\t\t\t\t\t\t\t<img src=\"";
                    if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_new_, "image"), "html", null, true);
                    echo "\" alt=\"";
                    if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_new_, "titre"), "html", null, true);
                    echo "\" class=\"image_news\" />
\t\t\t\t\t\t\t";
                } else {
                    // line 26
                    echo "\t\t\t\t\t\t\t\t<img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/leimznews/images/leimznews.jpg"), "html", null, true);
                    echo "\" alt=\"";
                    if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_new_, "titre"), "html", null, true);
                    echo "\" class=\"image_news\" />
\t\t\t\t\t\t\t";
                }
                // line 28
                echo "\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<a class=\"news_titre\" href=\"";
                // line 31
                if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_consulter", array("id" => $this->getAttribute($_new_, "id"))), "html", null, true);
                echo "\" >";
                if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_new_, "titre"), "html", null, true);
                echo "</a>
\t\t\t\t\t\t\t";
                // line 32
                $context["i"] = 0;
                // line 33
                echo "\t\t\t\t\t\t\t par 
\t\t\t\t\t\t\t ";
                // line 34
                if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($_new_, "auteur"));
                foreach ($context['_seq'] as $context["_key"] => $context["auteurs"]) {
                    // line 35
                    echo "\t\t\t\t\t\t\t ";
                    if (isset($context["i"])) { $_i_ = $context["i"]; } else { $_i_ = null; }
                    $context["i"] = ($_i_ + 1);
                    // line 36
                    echo "\t\t\t\t\t\t\t ";
                    if (isset($context["i"])) { $_i_ = $context["i"]; } else { $_i_ = null; }
                    if (($_i_ > 1)) {
                        // line 37
                        echo "\t\t\t\t\t\t\t , 
\t\t\t\t\t\t\t ";
                    }
                    // line 39
                    echo "\t\t\t\t\t\t\t <a class=\"news_auteur\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_profile_show"), "html", null, true);
                    echo "?pseudo=";
                    if (isset($context["auteurs"])) { $_auteurs_ = $context["auteurs"]; } else { $_auteurs_ = null; }
                    echo twig_escape_filter($this->env, $_auteurs_, "html", null, true);
                    echo "\" >";
                    if (isset($context["auteurs"])) { $_auteurs_ = $context["auteurs"]; } else { $_auteurs_ = null; }
                    echo twig_escape_filter($this->env, $_auteurs_, "html", null, true);
                    echo "</a>
\t\t\t\t\t\t\t ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['auteurs'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 40
                echo ".
\t\t\t\t\t\t\t<div class=\"news_date\">Le ";
                // line 41
                if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                echo twig_escape_filter($this->env, twig_date_format_filter($this->getAttribute($_new_, "date"), "d/m/Y"), "html", null, true);
                echo ". </div>
\t\t\t\t\t\t\t<div class=\"news_contenu\">";
                // line 42
                if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                echo $this->env->getExtension('fm_bbcode')->filter(twig_truncate_filter($this->env, $this->getAttribute($_new_, "contenu"), 300, true, "[...]"), "default_filter");
                echo " <a href=\"";
                if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("news_consulter", array("id" => $this->getAttribute($_new_, "id"))), "html", null, true);
                echo "\" >&gt;&gt; ";
                if (isset($context["new"])) { $_new_ = $context["new"]; } else { $_new_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_new_, "nbCommentaires"), "html", null, true);
                echo " personnes en parlent >></a></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['new'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 47
            echo "\t\t\t
\t\t\t</table>
\t\t\t\t";
            // line 49
            if ($this->env->getExtension('security')->isGranted("ROLE_SUPER_ADMIN")) {
                // line 50
                echo "\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_news_ajouter"), "html", null, true);
                echo "\" >Ajouter une news</a><br/>
\t\t\t\t\t
\t\t\t\t";
            }
            // line 53
            echo "\t\t\t\t
\t\t\t";
            // line 54
            if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
            if ((twig_length_filter($this->env, $_pagination_) > 5)) {
                // line 55
                echo "\t\t\t<div class=\"pagerfanta pagination\">
\t\t\t\t";
                // line 56
                if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
                echo $this->env->getExtension('pagerfanta')->renderPagerfanta($_pagination_, "default_translated");
                echo "
\t\t\t</div>
\t\t\t";
            }
            // line 59
            echo "\t\t";
        } else {
            // line 60
            echo "\t\t\t\t<p>Aucune news trouvée.</p>
\t\t";
        }
        // line 62
        echo "
";
    }

    public function getTemplateName()
    {
        return "LeimzNewsBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
