<?php

namespace Leimz\UtilisateurBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UtilisateurAjaxForm extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {        
        $builder->add('username', 'text');
    }
    
    public function getName()
    {        
        return 'utilisateurajax';
    }
}
