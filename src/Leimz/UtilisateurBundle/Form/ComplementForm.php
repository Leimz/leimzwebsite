<?php
namespace Leimz\UtilisateurBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ComplementForm extends AbstractType
{

	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder->add('signature', 'textarea', array('required' => false,'attr' => array('cols' => '70', 'rows' => '8',),));
		$builder->add('sexe', 'choice', array('choices' => array(
							'0' => 'Homme', '1' => 'Femme',
				),
				'expanded' => true,
				'multiple' => false,
				'required' => false,));
                $builder->add('pubEmail', 'checkbox', array('required' => false,));
		$builder->add('description', 'textarea', array('required' => false,'attr' => array('cols' => '70', 'rows' => '8',),));
		$builder->add('birthdate', 'birthday', array('required' => false,'format' => 'dd MM yyyy',));
		$builder->add('typeAvatar', 'choice', array('choices' => array(
							'0' => 'Entrez l\'URL d\'une image déjà hébergée', '1' => 'Sélectionnez un avatar dans la liste',
				),
				'expanded' => true,
				'multiple' => false,
				));
		$builder->add('avatar', 'text', array('required' => false,));
	}
	
	public function getName()
	{
		return 'complement';	
	}
	
}
