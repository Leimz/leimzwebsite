<?php 

namespace Leimz\UtilisateurBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class RemoveRoleForm extends AbstractType
{

	public function buildForm(FormBuilder $builder, array $options)
	{
		
		$builder->add('roles', 'choice', array('choices' => array('ROLE_SUPER_ADMIN' => 'ROLE_SUPER_ADMIN', 'ROLE_ADMIN' => 'ROLE_ADMIN')));
	
	}

	public function getName()
	{

		return 'removeroleform';	
	
	}	
	
}