<?php

namespace Leimz\UtilisateurBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class lockMembreAjaxForm extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {        
        $builder->add('id', 'hidden');
    }
    
    public function getName()
    {        
        return 'lockmembreajax';
    }
}
