<?php 

namespace Leimz\UtilisateurBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ProfileForm extends AbstractType
{

	public function buildForm(FormBuilder $builder, array $options)
	{
		
		$builder->add('username', 'text');
		$builder->add('email', 'email');
	
	}

	public function getName()
	{

		return 'profileform';	
	
	}	
	
}