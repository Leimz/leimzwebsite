<?php

namespace Leimz\UtilisateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Leimz\UtilisateurBundle\Form\UtilisateurAjaxForm;
use Leimz\UtilisateurBundle\Form\lockMembreAjaxForm;
use Leimz\UtilisateurBundle\Form\ProfileForm;
use Leimz\UtilisateurBundle\Form\AddRoleForm;
use Leimz\UtilisateurBundle\Form\RemoveRoleForm;


class AdminController extends Controller
{
    
    public function listerMembresAction()
    {
        
        
        $form = $this->container->get('form.factory')->create(new UtilisateurAjaxForm());
        
  
        $membres = (isset($membres))? $membres : $this->container->get('doctrine')->getEntityManager()->getRepository('LeimzUtilisateurBundle:Utilisateur')->findByLocked(0);
	
			$locked = 0;	
	
		return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:listerMembres.html.twig', array(
								 'membres' => $membres,
								'form' => $form->createView(),
								'locked' => $locked,
								));
        
    }
    
    public function listerMembresLockedAction()
    {
    
    	$form = $this->container->get('form.factory')->create(new UtilisateurAjaxForm());
        
  
        $membres = (isset($membres))? $membres : $this->container->get('doctrine')->getEntityManager()->getRepository('LeimzUtilisateurBundle:Utilisateur')->findByLocked(1);
	
			$locked = 1;	
	
		return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:listerMembres.html.twig', array(
								 'membres' => $membres,
								'form' => $form->createView(),
								'locked' => $locked,
								));
    	
    }
    
    public function rechercherMembreAction()
	{               
    	$request = $this->container->get('request');

    	if($request->isXmlHttpRequest())
    	{
       	$user = '';
  			$user = $request->request->get('motcle');
  			$locked = $request->request->get('locked');

       	$em = $this->container->get('doctrine')->getEntityManager();

        	if($user != '')
        	{
               $qb = $em->createQueryBuilder();

               $qb->select('a')
                  ->from('LeimzUtilisateurBundle:Utilisateur', 'a')
                  ->where("a.username LIKE :user AND a.locked = :locked")
                  ->orderBy('a.username', 'ASC')
                  ->setParameter('user', '%'.$user.'%')
                  ->setParameter('locked', $locked);

               $query = $qb->getQuery();               
               $membres = $query->getResult();
        	}
        	else {
            	$membres = $em->getRepository('LeimzUtilisateurBundle:Utilisateur')->findByLocked($locked);
        	}

        	return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:listeMembres.html.twig', array(
           		 'membres' => $membres
            		));
    	}
    	else {
        	return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:listeMembres.html.twig', array(
            		));
    	} 
	}
	
	public function lockMembreAction($membre)
	{
		
		$em = $this->container->get('doctrine')->getEntityManager();
		
		$membre = \strtolower($membre);
		
		$qb = $em->createQueryBuilder();
		$qb->select('a')
				->from('LeimzUtilisateurBundle:Utilisateur', 'a')
				->where('a.usernameCanonical = :membre')
				->setParameter('membre' , $membre);
				
		$query = $qb->getQuery();               
      $membre = $query->getResult();
               
               
		$form = $this->container->get('form.factory')->create(new lockMembreAjaxForm(), $membre);
		
		if($membre)
		{
			
			return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:lockMembre.html.twig', array(
								'membre' => $membre,
								'form' => $form->createView(),
								));	
		}
		else {
	
		return $this->redirect($this->generateUrl('admin_voir_membres'));
		
		}
		
	}
	
	public function lockMembreAjaxAction()
	{
		
		$request = $this->container->get('request');
		
		if($request->isXmlHttpRequest())
		{
				
			$em = $this->container->get('doctrine')->getEntityManager();			
			$membre = $request->request->get('membre');
			$action = $request->request->get('action');
			$done = 0; //si une action a été effectuée ou non
			
			$membre = $em->getRepository('LeimzUtilisateurBundle:Utilisateur')->findOneByUsernameCanonical($membre);
			
			foreach($membre as $membreBis){ $membre = $membreBis; }

			$form = $this->container->get('form.factory')->create(new lockMembreAjaxForm(), $membre);
			
			if($action == true)
			{
			$locked = ($membre->getLocked() == 1)? 0 : 1;
			
			$membre->setLocked($locked);
			
			$em->persist($membre);
			$em->flush();
			
			$done = 1;
			
			 }
							
			return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:lockMembreAjax.html.twig', array(
           		 'membre' => $membre,
           		 'form' => $form->createView(),
           		 'done' => $done,
            		));
			
		}
		
		
	}
	
	public function modifierMembreAction($membre)
	{
		
		$username = \strtolower($membre);
		$erreur = 0;
		$em = $this->getDoctrine()->getEntityManager();
		$membre = $em->getRepository('LeimzUtilisateurBundle:Utilisateur')->findOneByUsernameCanonical($username);
		
		if($membre == '')
		{
			return $this->redirect($this->generateUrl('admin_voir_membres'));
		}		
		
		$form = $this->container->get('form.factory')->create(new ProfileForm(), $membre);
		$request = $this->container->get('request');
		
		if($request->getMethod() == 'POST')
		{
			$form->bindRequest($request);

			if($form->isValid())
			{
				$em = $this->container->get('doctrine')->getEntityManager();
				$em->persist($membre);
				$em->flush();
				
				return $this->redirect($this->generateUrl('profile_show_member', array('username' => $membre->getUsername()) ) );
			}
			
		}
		
		
		return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:modifierMembre.html.twig', array(
           		 'membre' => $membre,
           		 'form' => $form->createView(),
            		));
	}
	
	public function editRolesAction($membre)
	{
		
			$form = $this->container->get('form.factory');
			
			$username = $membre;
			$membre = $this->container->get('doctrine')->getEntityManager()->getRepository('LeimzUtilisateurBundle:Utilisateur')->findOneByUsernameCanonical(strtolower($username));
			
			$formAdd = $form->create(new AddRoleForm());
			$formRm = $form->create(new RemoveRoleForm());
			
			
			
			return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:editRolesMembre.html.twig', array(
												'membre' => $membre,
												'formAdd' => $formAdd->createView(),
												'formRm' => $formRm->createView(),
												));
		
	}
	
	public function addRoleAjaxAction()
	{
		
			$form = $this->container->get('form.factory');
			$formAdd = $form->create(new AddRoleForm());
			$message = 0;
			
			$request = $this->container->get('request');
			
			if($request->isXmlHttpRequest())
			{
				
				$role = $request->request->get('role');
				$membre =  $request->request->get('membre');
				$em = $this->container->get('doctrine')->getEntityManager();
				$membre = $em->getRepository('LeimzUtilisateurBundle:Utilisateur')->findOneByUsernameCanonical(strtolower($membre));
				
				if($membre->hasRole($role))
				{}
				else 
				{
					 
					 $membre->addRole($role);
					 $em->persist($membre);
					$em->flush();
					 
				}
			}
			
			return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:editRoleAddForm.html.twig', array(
									'formAdd' => $formAdd->createView(),
									));
		
	}
	
	public function removeRoleAjaxAction()
	{
		
			$form = $this->container->get('form.factory');
			$formRm = $form->create(new RemoveRoleForm());
			
			$request = $this->container->get('request');
			
			if($request->isXmlHttpRequest())
			{
				
				$role = $request->request->get('role');
				$membre =  $request->request->get('membre');
				$em = $this->container->get('doctrine')->getEntityManager();
				$membre = $em->getRepository('LeimzUtilisateurBundle:Utilisateur')->findOneByUsernameCanonical(strtolower($membre));
				
				if($membre->hasRole($role))
				{
							 
					 $membre->removeRole($role);
					 $em->persist($membre);
					$em->flush();
					
				}
			
			}
			return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:editRoleRemoveForm.html.twig', array(
									'formRm' => $formRm->createView(),
									));
		
	}
	
	public function displayRolesAction()
	{
		
		$request = $this->container->get('request');
		if($request->isXmlHttpRequest())
		{
		
			$membre = $request->request->get('membre');
			
		}
		$em = $this->container->get('doctrine')->getEntityManager();
		$membre = $em->getRepository('LeimzUtilisateurBundle:Utilisateur')->findOneByUsernameCanonical(strtolower($membre));
		
		return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Admin:displayRoles.html.twig', array(
												'membre' => $membre, ));
		
	}

}
