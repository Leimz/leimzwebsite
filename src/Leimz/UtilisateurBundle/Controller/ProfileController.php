<?php

namespace Leimz\UtilisateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\Controller\ProfileController as baseProfile;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class ProfileController extends baseProfile
{
    
    public function showMemberAction($username)
    {
        
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $membre = $em->getRepository('LeimzUtilisateurBundle:Utilisateur')->findOneByUsername($username);
        
        return $this->container->get('templating')->renderResponse('LeimzUtilisateurBundle:Profile:showMember.html.twig', array('user' => $membre,
                                                                                            ));
    }
    
   
}

