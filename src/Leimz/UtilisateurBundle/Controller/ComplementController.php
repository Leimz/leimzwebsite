<?php
namespace Leimz\UtilisateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Leimz\UtilisateurBundle\Form\ComplementForm;
use Leimz\UtilisateurBundle\Entity\Complement;


class ComplementController extends Controller
{
	
	public function modifierComplementAction()
	{
		
		$user = $this->container->get('security.context')->getToken()->getUser();
		$userDoc = $this->container->get('doctrine')->getEntityManager();
		$complement_user = ($user->getComplement() instanceof Complement)? $user->getComplement() : new Complement();
		
		$form = $this->container->get('form.factory')->create(new ComplementForm(), $complement_user);
		
		$request = $this->container->get('request');
		
		if($request->getMethod() == 'POST')
		{
			$form->bindRequest($request);
		
			if($form->isValid())
			{
				$em = $this->container->get('doctrine')->getEntityManager();
				$em->persist($complement_user);
				$user->setComplement($complement_user);
				$em->persist($user);
				
				$em->flush();
		
				return $this->redirect($this->container->get('router')->generate('profile_show'));
					
			}
		}
		
		return $this->render('LeimzUtilisateurBundle:Complement:modifier.html.twig', array(
								'form' => $form->createView(),
								));
		
	}

}
