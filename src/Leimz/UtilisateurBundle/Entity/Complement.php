<?php

namespace Leimz\UtilisateurBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Leimz\UtilisateurBundle\Entity\Complement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Leimz\UtilisateurBundle\Entity\ComplementRepository")
 */
class Complement
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $avatar
     *
     * @ORM\Column(name="avatar", type="string", length=511)
     */
    private $avatar;
    
    /**
     * @var boolean $typeAvatar
     * 
     * @ORM\Column(name="type_avatar", type="boolean")
     */
    private $typeAvatar;

    /**
     * @var text $signature
     *
     * @ORM\Column(name="signature", type="text")
     */
    private $signature;

    /**
     * @var boolean $sexe
     *
     * @ORM\Column(name="sexe", type="boolean")
     */
    private $sexe;

    /**
     * @var datetime $birthdate
     *
     * @ORM\Column(name="birthdate", type="datetime")
     */
    private $birthdate;

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     * @var boolean $pubEmail
     *
     * @ORM\Column(name="pubEmail", type="boolean")
     */
    private $pubEmail;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * Get avatar
     *
     * @return string 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set signature
     *
     * @param text $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    }

    /**
     * Get signature
     *
     * @return text 
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set sexe
     *
     * @param boolean $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }

    /**
     * Get sexe
     *
     * @return boolean 
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set birthdate
     *
     * @param datetime $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * Get birthdate
     *
     * @return datetime 
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set typeAvatar
     *
     * @param boolean $typeAvatar
     */
    public function setTypeAvatar($typeAvatar)
    {
        $this->typeAvatar = $typeAvatar;
    }

    /**
     * Get typeAvatar
     *
     * @return boolean 
     */
    public function getTypeAvatar()
    {
        return $this->typeAvatar;
    }
    
    /**
     * Set pubEmail
     *
     * @param boolean $sexe
     */
    public function setPubEmail($pubEmail)
    {
        $this->pubEmail = $pubEmail;
    }

    /**
     * Get sexe
     *
     * @return boolean 
     */
    public function getPubEmail()
    {
        return $this->pubEmail;
    }

}