<?php
namespace Leimz\UtilisateurBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Utilisateur extends BaseUser
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\generatedValue(strategy="AUTO")
    */
    protected $id;

	 /**
     * @ORM\ManyToMany(targetEntity="Leimz\UtilisateurBundle\Entity\Groupe")
     * @ORM\JoinTable(name="fos_user_user_groupe",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    public function __construct()
    {
        parent::__construct();
    }
    
    
    /**
     * @ORM\OneToOne(targetEntity="Leimz\UtilisateurBundle\Entity\Complement")
     */
    private $complement;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add groups
     *
     * @param Leimz\UtilisateurBundle\Entity\Groupe $groups
     */
    public function addGroupe(\Leimz\UtilisateurBundle\Entity\Groupe $groups)
    {
        $this->groups[] = $groups;
    }

    /**
     * Get groups
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->groups;
    }
    
    public function getLocked()
    {
    
			return $this->locked;
    
    }

    /**
     * Set complement
     *
     * @param Leimz\UtilisateurBundle\Entity\Complement $complement
     */
    public function setComplement(\Leimz\UtilisateurBundle\Entity\Complement $complement)
    {
        $this->complement = $complement;
    }

    /**
     * Get complement
     *
     * @return Leimz\UtilisateurBundle\Entity\Complement 
     */
    public function getComplement()
    {
        return $this->complement;
    }
}