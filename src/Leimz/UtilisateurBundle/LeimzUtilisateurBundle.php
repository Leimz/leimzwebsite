<?php

namespace Leimz\UtilisateurBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LeimzUtilisateurBundle extends Bundle
{
	public function getParent()
        {
            return('FOSUserBundle');
        }
}
