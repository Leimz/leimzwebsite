<?php

namespace Leimz\PagesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class WhatsthatForm extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('contenu', 'textarea')
            ->add('visible', 'checkbox', array('required' => false,))
        ;
    }

    public function getName()
    {
        return 'whatsthat_form';
    }
}
