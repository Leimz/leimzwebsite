<?php

namespace Leimz\PagesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction($name)
    {
        return $this->render('LeimzPagesBundle:Default:index.html.twig', array('name' => $name));
    }
}
