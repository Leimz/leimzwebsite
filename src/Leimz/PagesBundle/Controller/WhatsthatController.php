<?php

namespace Leimz\PagesBundle\Controller;

use Leimz\PagesBundle\Form\WhatsthatForm;
use Leimz\PagesBundle\Entity\Whatsthat;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class WhatsthatController extends Controller
{
    
    public function indexAction()
    {
        
        $em = $this->get('doctrine')->getEntityManager();
        
        $parties = $em->getRepository('LeimzPagesBundle:Whatsthat')->findAllInOrder();
        
        return $this->render('LeimzPagesBundle:Whatsthat:index.html.twig', array(
                                            'parties' => $parties,
        ));
    }
    
    public function ajouterAction($id = null)
    {
        $em = $this->get('doctrine')->getEntityManager();
        
        if($id != null)
        {
            
            $partie = $em->getRepository('LeimzPagesBundle:Whatsthat')->findOneById($id);
            
            if($partie == null)
            {
                throw NotFoundHttpException();
            }
        }
        else
        {
            $partie = new Whatsthat();
        }
        
        $form = $this->container->get('form.factory')->create(new WhatsthatForm(), $partie);
        
        $request = $this->container->get('request');
        
        if($request->getMethod() == 'POST')
		{
			
			$form->bindRequest($request);

			if($form->isValid())
			{   
                            if($id == null)
                            {
                                $partie->setOrdre($em->getRepository('LeimzPagesBundle:Whatsthat')->getOrdreMax()->getOrdre()+1);
                                $partie->setDate(new \DateTime());
                            }
                            $em->persist($partie);
                            $em->flush();
                            
                            $this->redirect($this->container->get('router')->generate('whatsthat_default'));
                            
                        }
                        
                }
                
          return $this->render('LeimzPagesBundle:Whatsthat:ajouter.html.twig', array(
                                            'form' => $form->createView(),
          ));
          
    }
}