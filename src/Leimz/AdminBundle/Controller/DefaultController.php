<?php

namespace Leimz\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction()
    {
        return $this->render('LeimzAdminBundle:Default:index.html.twig', array());
    }
}
