<?php

namespace Leimz\NewsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Leimz\NewsBundle\Entity\Commentaire as Commentaire;
use Leimz\NewsBundle\Form\CommentaireForm as CommentaireForm;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class CommentaireController extends Controller
{

    public function ajouterAction($id = null, $news = null)
    {

		$em = $this->container->get('doctrine')->getEntityManager();
		$user = $this->get('security.context')->getToken()->getUser();

		if(isset($id) && !isset($news))
		{
			$commentaire = $em->find('LeimzNewsBundle:Commentaire', $id);

			if(!$commentaire)
			{
				throw new NotFoundHttpException('Le commentaire n\'a pas été trouvé.');
				
			}
			if($commentaire->getAuteur() != $user && !$this->get('security.context')->isGranted('ROLE_ADMIN'))
			{
				throw new AccessDeniedHttpException('Vous n\'avez pas le droit de modifier ce commentaire.');
			}
			
			$news = $commentaire->getNews();
			$edition = true;
		}
		else
		{
			$commentaire = new Commentaire();
			$edition = false;

			$news = $em->find('LeimzNewsBundle:News', $news);

			$date = new \DateTime();

			$commentaire->setDate($date);
			$commentaire->setDateEdit($date);
			$commentaire->setNews($news);
			$commentaire->setAuteur($user);
			$commentaire->setSupprimer(false);
		}
		
		
		if($news->getLocked() == 1)
		{
			throw new AccessDeniedHttpException('Les commentaires de la news sont verrouillés. Impossible d\'accéder au formulaire.');
			
		}

		$form = $this->container->get('form.factory')->create(new CommentaireForm(), $commentaire);

		$request = $this->container->get('request');

		if($request->getMethod() == 'POST')
		{
			$form->bindRequest($request);

			if($form->isValid())
			{
				$em = $this->container->get('doctrine')->getEntityManager();
				$em->persist($commentaire);

				if(!isset($id))
				{
					$news->setNbCommentaires($news->getNbCommentaires() + 1);
					$em->persist($news);
				}

				$em->flush();
				$nbCommentaire = $news->getNbCommentaires();
				$page = floor($nbCommentaire / 15)+1;
				
				return $this->redirect($this->container->get('router')->generate('news_consulter', array('id' => $news->getId(),)).'?page='.$page.'');
			
			}
		}

        return $this->container->get('templating')->renderResponse('LeimzNewsBundle:Commentaire:ajouter.html.twig',
										array(
											'form' => $form->createView(),
											'edition' => $edition,
										));
    }


}
