<?php

namespace Leimz\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Leimz\NewsBundle\Entity\News;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;


class DefaultController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $news = $em->getRepository('LeimzNewsBundle:News')->findBy(array('supprimer' => 0, "brouillon" => 0,));

        $pagination = new Pagerfanta(new ArrayAdapter($news));
        $pagination->setMaxPerPage(5);
        $pagination->setCurrentPage($this->get('request')->query->get('page', 1), false, true);

        return $this->render('LeimzNewsBundle:Default:index.html.twig', array(
									'news' => $news,
									'pagination' => $pagination,
									 ));	
    }
}
