<?php

namespace Leimz\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Leimz\NewsBundle\Entity\News;
use Leimz\NewsBundle\Form\NewsForm;



class AdminController extends Controller
{

    public function ajouterAction($id = null)
    {

		$em = $this->container->get('doctrine')->getEntityManager();
		$date = new \DateTime();
		$user = $this->get('security.context')->getToken()->getUser();
		
		$message1 = 0;
		$message2 = 0;
		$message3 = 0;
		$type = 0;

		if(isset($id))
		{
			$news = $em->find('LeimzNewsBundle:News', $id);

			if(!$news)
			{
				$message1 = 1;// news pas trouvée
			}
			else 
			{
				
				$news->setDateEdit($date);
				if(!$news->isAuteur($user))
				{
					$news->addAuteur($user);
				}
				
			
			}
		}
		else
		{
			$news = new News();

			$news->setDate($date);
			$news->setDateEdit($date);
			$news->addAuteur($user);
			$news->setLocked(false);
			$news->setSupprimer(false);
			$news->setNbCommentaires(0);
			if($news->getImage() == null) {
				$news->setImage('default');
				}
			
				$type = 1;	
		}

		$form = $this->container->get('form.factory')->create(new NewsForm(), $news);

		$request = $this->container->get('request');

		if($request->getMethod() == 'POST')
		{
			
			$form->bindRequest($request);

			if($form->isValid())
			{
				$em = $this->container->get('doctrine')->getEntityManager();
				$em->persist($news);
				$em->flush();
				if (isset($id)) $message2 = 1;
				else $message3 = 1;
				
				if($news->getBrouillon() == 0)
				{
					return $this->redirect($this->container->get('router')->generate('news_consulter', array('id' => $news->getId(),)));
				}
				else
				{
					return $this->redirect($this->container->get('router')->generate('admin_news_previsualisation', array('news' => $news->getId(),)));
						
				}
			}
		}
		
		

        return $this->container->get('templating')->renderResponse('LeimzNewsBundle:Admin:ajouter.html.twig',
										array(
											'form' => $form->createView(),
											'message1' => $message1,
											'message2' => $message2,
											'message3' => $message3,
											'type' => $type,
											'news' => $news,
										));
		}

    public function supprimerAction($id, $confirm = null)
    {
		$em = $this->container->get('doctrine')->getEntityManager();
		$news = $em->find('LeimzNewsBundle:News', $id);
		$message = '';

		$confirm = (isset($confirm))? $confirm : 0;

		if(!$news)
		{
			$message = 'La news n\'a pas été trouvée.';
			$confirm = 2;
		}
		elseif($confirm == 1)
		{
			$news->setSupprimer(1);
			$news->setBrouillon(0);
			$em->persist($news);
			$em->flush();
			$message = 'La news a bien été supprimée';
		}

    	return $this->render('LeimzNewsBundle:Admin:supprimer.html.twig', array(
							'message' => $message,
							'news' => $news,
							'confirm' => $confirm
							));
    }

    public function verrouillerAction($id, $confirm = null)
    {

		$em = $this->container->get('doctrine')->getEntityManager();
		$news = $em->find('LeimzNewsBundle:News', $id);

		$message = '';

		if(!$news)
		{
			$message = 'La News demandée est introuvable.';
		}
		elseif( isset($confirm) )
		{
			if($news->getLocked() == true)
			{
				$news->setLocked(false);

				$message = 'La News a bien été déverrouillée.';
			}
			else
			{
				$news->setLocked(true);

				$message = 'La news a bien été verrouillée.';
			}

			$em->persist($news);
			$em->flush();
		}


    	return $this->render('LeimzNewsBundle:Admin:verrouiller.html.twig', array(
								'news' => $news,
								'message' => $message
								));
    }

    public function supprimerCommentaireAction($id, $confirm = null)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();
		$commentaire = $em->find('LeimzNewsBundle:Commentaire', $id);
		$message = '';

		$confirm = (isset($confirm))? $confirm : 0;

		if(!$commentaire)
		{
			$message = 'Le commentaire n\'a pas été trouvé.';
			$confirm = 2;
		}
		elseif($confirm == 1)
		{
			$commentaire->setSupprimer(1);
			$em->persist($commentaire);
			$em->flush();
			$message = 'Le commentaire a bien été supprimé.';
		}

    	return $this->render('LeimzNewsBundle:Admin:commentaire.html.twig', array(
							'message' => $message,
							'commentaire' => $commentaire,
							'confirm' => $confirm
							));
    }

	public function brouillonsAction()
	{
		$em = $this->container->get('doctrine')->getEntityManager();
		
		$news = $em->getRepository('LeimzNewsBundle:News')->findBy(array( 'brouillon' => 1, ));
		
		return $this->render('LeimzNewsBundle:Admin:brouillons.html.twig', array(
									'news' => $news,
				));
	}
	
	public function previsualisationAction($news)
	{
		$em = $this->container->get('doctrine')->getEntityManager();
		$news = $em->find('LeimzNewsBundle:News', $news);
		
		return $this->render('LeimzNewsBundle:News:consulter.html.twig', array(
							'new' => $news,
							));
	}
	
	public function voirSupprimeesAction()
	{
		$em = $this->container->get('doctrine')->getEntityManager();
		
		$supprimees = $em->find('LeimzNewsBundle:News', array('supprimer' => 1,));
		
		return $this->render('LeimzNewsBundle:Admin:voirSupprimees.html.twig', array(
								'supprimees' => $supprimees,
				));
	}
}

