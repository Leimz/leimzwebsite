<?php

namespace Leimz\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;


class NewsController extends Controller
{

    public function consulterAction($id)
    {
    	$em = $this->get('doctrine')->getEntityManager();

    	$new = $em->getRepository('LeimzNewsBundle:News')->find(array('id' => $id, 'supprimer' => 0, 'brouillon' => 0,));
    	
    	$commentaires = $new->getCommentairesArray();
    	
    	$pagination = new Pagerfanta(new ArrayAdapter($commentaires));
    	$pagination->setMaxPerPage(10);
    	$pagination->setCurrentPage($this->get('request')->query->get('page', 1), false, true);

        return $this->render('LeimzNewsBundle:News:consulter.html.twig', array(
        								'new' => $new,
        								'pagination' => $pagination,
        		));
    }

	public function parTagAction($tag)
	{
		return $this->render('LeimzNewsBundle:Default:index.html.twig');
	}

}
