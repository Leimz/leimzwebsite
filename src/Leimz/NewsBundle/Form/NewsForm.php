<?php
namespace Leimz\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class NewsForm extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder->add('titre')
				->add('Image', 'text', array( 'required' => false, ))
				->add('contenu', 'textarea', array( 'attr' => array( 'cols' => 70, 'rows' => 12,)))
				->add('brouillon', 'checkbox',array( 'required' => false,))
				;


	}

	public function getName()
	{
		return 'news';
	}
}