<?php

namespace Leimz\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CommentaireForm extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder->add('contenu', 'textarea', array('attr' => array('cols' => 70, 'rows' => 12,),
							))
				;


	}

	public function getName()
	{
		return 'commentaire';
	}
}