<?php

namespace Leimz\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Leimz\NewsBundle\Entity\News
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Leimz\NewsBundle\Entity\NewsRepository")
 */
class News
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $titre
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\ManyToMany(targetEntity="Leimz\UtilisateurBundle\Entity\Utilisateur")
     *
     */

     private $auteur;

    /**
     * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="news", cascade={"remove", "persist"})
     */

     private $commentaire;

     /**
      * @ORM\Column(name="contenu", type="text")
      */

      private $contenu;

    /**
     * @var date $date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var date $date_edit
     *
     * @ORM\Column(name="date_edit", type="date")
     */
    private $date_edit;

    /**
     *
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    // public $fileImage;

    /**
     * @var integer $nb_commentaires
     *
     * @ORM\Column(name="nb_commentaires", type="integer")
     */
    private $nb_commentaires;

    /**
     * @var boolean $supprimer
     *
     * @ORM\Column(name="supprimer", type="boolean")
     */
    private $supprimer;

    /**
     * @var boolean $locked
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked;
    
    /**
     * @var boolean $brouillon
     *
     * @ORM\Column(name="brouillon", type="boolean")
     */
    private $brouillon;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set date
     *
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date_edit
     *
     * @param date $dateEdit
     */
    public function setDateEdit($dateEdit)
    {
        $this->date_edit = $dateEdit;
    }

    /**
     * Get date_edit
     *
     * @return date
     */
    public function getDateEdit()
    {
        return $this->date_edit;
    }

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set nb_commentaires
     *
     * @param integer $nbCommentaires
     */
    public function setNbCommentaires($nbCommentaires)
    {
        $this->nb_commentaires = $nbCommentaires;
    }

    /**
     * Get nb_commentaires
     *
     * @return integer
     */
    public function getNbCommentaires()
    {
        return $this->nb_commentaires;
    }

    /**
     * Set supprimer
     *
     * @param boolean $supprimer
     */
    public function setSupprimer($supprimer)
    {
        $this->supprimer = $supprimer;
    }

    /**
     * Get supprimer
     *
     * @return boolean
     */
    public function getSupprimer()
    {
        return $this->supprimer;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }
    public function __construct()
    {
        $this->commentaire = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add commentaire
     *
     * @param Leimz\NewsBundle\Entity\Commentaire $commentaire
     */
    public function addCommentaire(\Leimz\NewsBundle\Entity\Commentaire $commentaire)
    {
        $this->commentaire[] = $commentaire;
    }

    /**
     * Get commentaire
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set contenu
     *
     * @param text $contenu
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }

    /**
     * Get contenu
     *
     * @return text
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set auteur
     *
     * @param Leimz\UtilisateurBundle\Entity\Utilisateur $auteur
     */
    public function addAuteur(\Leimz\UtilisateurBundle\Entity\Utilisateur $auteur)
    {
        if(!$this->getAuteur()->contains($auteur)){
        	$this->getAuteur()->add($auteur);
        }
        
        return $this;
    }

    /**
     * Get auteur
     *
     * @return Collection
     */
    public function getAuteur()
    {
        return $this->auteur ?: $this->auteur = new ArrayCollection();
    }
    
    /**
     * Set brouillon
     *
     * @param boolean $brouillon
     */
    public function setBrouillon($brouillon)
    {
    	$this->brouillon = $brouillon;
    }
    
    /**
     * Get brouillon
     *
     * @return boolean
     */
    public function getBrouillon()
    {
    	return $this->brouillon;
    }
    
    public function isAuteur($user)
    {
    	return in_array($user, $this->getAuteurArray());
    }
    
    /**
     *
     * @return array
     */
    public function getAuteurArray()
    {
    	$auteurs = array();
    	foreach ($this->getAuteur() as $auteur) {
    		$auteurs[] = $auteur;
    	}
    
    	return $auteurs;
    }
    
    /**
     *
     * @return array
     */
    public function getCommentairesArray()
    {
    	$commentaires = array();
    	foreach ($this->getCommentaire() as $commentaire) {
    		$commentaires[] = $commentaire;
    	}
    
    	return $commentaires;
    }
}