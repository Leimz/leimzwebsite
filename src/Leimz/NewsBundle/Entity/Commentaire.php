<?php

namespace Leimz\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Leimz\NewsBundle\Entity\Commentaire
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Leimz\NewsBundle\Entity\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text $contenu
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @ORM\ManyToOne(targetEntity="News", inversedBy="commentaire", cascade={"remove"})
     * @ORM\JoinColumn(name="news_id", referencedColumnName="id")
     */

     private $news;

     /**
      * @ORM\ManyToOne(targetEntity="Leimz\UtilisateurBundle\Entity\Utilisateur", cascade={"remove"})
      * @ORM\JoinColumn(name="auteur_commentaire", referencedColumnName="id")
      */

	private $auteur;

    /**
     * @var date $date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var date $date_edit
     *
     * @ORM\Column(name="date_edit", type="date")
     */
    private $date_edit;

    /**
     * @var boolean $supprimer
     *
     * @ORM\Column(name="supprimer", type="boolean")
     */
    private $supprimer;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param text $contenu
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }

    /**
     * Get contenu
     *
     * @return text
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set date
     *
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date_edit
     *
     * @param date $dateEdit
     */
    public function setDateEdit($dateEdit)
    {
        $this->date_edit = $dateEdit;
    }

    /**
     * Get date_edit
     *
     * @return date
     */
    public function getDateEdit()
    {
        return $this->date_edit;
    }

    /**
     * Set supprimer
     *
     * @param boolean $supprimer
     */
    public function setSupprimer($supprimer)
    {
        $this->supprimer = $supprimer;
    }

    /**
     * Get supprimer
     *
     * @return boolean
     */
    public function getSupprimer()
    {
        return $this->supprimer;
    }

    /**
     * Set news
     *
     * @param Leimz\NewsBundle\Entity\News $news
     */
    public function setNews(\Leimz\NewsBundle\Entity\News $news)
    {
        $this->news = $news;
    }

    /**
     * Get news
     *
     * @return Leimz\NewsBundle\Entity\News
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Set auteur
     *
     * @param Leimz\UtilisateurBundle\Entity\Utilisateur $auteur
     */
    public function setAuteur(\Leimz\UtilisateurBundle\Entity\Utilisateur $auteur)
    {
        $this->auteur = $auteur;
    }

    /**
     * Get auteur
     *
     * @return Leimz\UtilisateurBundle\Entity\Utilisateur 
     */
    public function getAuteur()
    {
        return $this->auteur;
    }
}